'use strict';

var path = require('path');
var jetpack = require('fs-jetpack');
var rollup = require('rollup').rollup;
var typescript = require('rollup-plugin-typescript');
var nodeResolve = require('rollup-plugin-node-resolve');
var commonjs = require('rollup-plugin-commonjs');

var nodeBuiltInModules = ['assert', 'buffer', 'child_process', 'cluster',
    'console', 'constants', 'crypto', 'dgram', 'dns', 'domain', 'events',
    'fs', 'http', 'https', 'module', 'net', 'os', 'path', 'process', 'punycode',
    'querystring', 'readline', 'repl', 'stream', 'string_decoder', 'timers',
    'tls', 'tty', 'url', 'util', 'v8', 'vm', 'zlib'];

var electronBuiltInModules = ['electron'];

var npmModulesUsedInApp = function () {
    var appManifest = require('../app/package.json');
    return Object.keys(appManifest.dependencies);
};

var generateExternalModulesList = function () {
    return [].concat(nodeBuiltInModules, electronBuiltInModules, npmModulesUsedInApp());
};

var cached = {};

class RollupRx {
    constructor(options){
        this.options = options;
    }
    resolveId( id ){
        if(id.startsWith('rxjs/')){
            return `${__dirname}/../app/node_modules/rxjs-es/${id.replace('rxjs/', '')}.js`;
        }
    }
}
module.exports = function (src, dest) {
    let config = {
        plugins: [
            // commonjs({
            //   include: 'node_modules/rxjs/**'
            // }),
            new RollupRx(),
            nodeResolve({ jsnext: true, main: true }),
            // commonjs({
            //   include: '${__dirname}/../app/node_modules/rxjs/**'
            // })
        ],
        format: 'cjs'
        // format: 'iife'
    };
    if (src.match(/\.ts$/)) {
        config.plugins.unshift(typescript({
            tsconfig: false,
            emitDecoratorMetadata: true,
            experimentalDecorators: true,
            // moduleResolution: "node",
            sourceMap: true,
            target: 'es6'
        }));
    }
    return rollup({
        entry: src,
        external: generateExternalModulesList(),
        cache: cached[src],
        plugins: config.plugins
    })
    .then(function (bundle) {
        cached[src] = bundle;

        var jsFile = path.basename(dest);
        var result = bundle.generate({
            format: config.format,
            sourceMap: true,
            sourceMapFile: jsFile,
            // Wrap code in self invoking function so the variables don't
            // pollute the global namespace.
            banner: '(function () {',
            footer: '\n}());'
        });
        return Promise.all([
            jetpack.writeAsync(dest, result.code + '\n//# sourceMappingURL=' + jsFile + '.map'),
            jetpack.writeAsync(dest + '.map', result.map.toString()),
        ]);
    });
};
