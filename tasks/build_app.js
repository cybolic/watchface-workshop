'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var batch = require('gulp-batch');
var plumber = require('gulp-plumber');
var jetpack = require('fs-jetpack');
var bundle = require('./bundle');
var utils = require('./utils');
var ts = require('gulp-typescript');
var concat = require('gulp-concat');

var projectDir = jetpack;
var srcDir = jetpack.cwd('./src');
var destDir = jetpack.cwd('./app');

var tsProject = ts.createProject('tsconfig.json');
// var tsProject = ts.createProject('tsconfig.json', {
//     module: 'es6',
//     outFile: undefined
// });
// var tsProject = ts.createProject(
// {
//     // outFile: 'app.js',
//     // declaration: true,
//     emitDecoratorMetadata: true,
//     experimentalDecorators: true,
//     sourceMap: true,
//     target: "es6",
//     moduleResolution: "node",
//     allowJs: true,
//     isolatedModules: true,
//     "paths": {
//         "*": [
//             "*",
//             destDir.path('*'),
//             destDir.path('node_modules/*'),
//             srcDir.path('*'),
//             srcDir.path('node_modules/*'),
//             "/usr/lib/node_modules/*",
//             "/usr/lib/nodejs/*"

//         ]
//     }
// });

var buildTypescript = function (src, dest) {
    var tsResult = gulp.src(src)
        .pipe(plumber())
        .pipe(tsProject());

    return Promise.all([ // Merge the two output streams, so this task is finished when the IO of both operations is done.
        // tsResult.dts.pipe(gulp.dest(destDir.path('definitions'))),
        tsResult.js
            .pipe(concat('ts-bundle.js'))
            .pipe(gulp.dest(dest))
    ]);
    // var tsResult = tsProject.src() // instead of gulp.src(...)
    //     .pipe(tsProject());

    // return Promise.all([
    //     tsResult.js.pipe(gulp.dest(dest))
    // ]);
}

gulp.task('bundle', function () {
    return Promise.all([
        bundle(srcDir.path('background.ts'), destDir.path('background.js')),
        // bundle(srcDir.path('app.js'), destDir.path('app.js')),
        // This isn't actually used, it's just run because rollup-typescript doesn't report warnings :(
        buildTypescript(srcDir.path('app.ts'), destDir.cwd()),
        bundle(srcDir.path('app.ts'), destDir.path('app.js')),
        bundle(srcDir.path('window-properties.ts'), destDir.path('window-properties.js')),
    ]);

    // var tsResult = gulp.src(srcDir.path('app.ts'))
    //         .pipe(tsProject());

    //     return merge([ // Merge the two output streams, so this task is finished when the IO of both operations is done.
    //         tsResult.dts.pipe(gulp.dest(destDir.path('definitions')),
    //         tsResult.js.pipe(gulp.dest(destDir.path('app.js')))
    //     ]);
    // ]);
});

gulp.task('less', function () {
    return gulp.src(srcDir.path('stylesheets/main.less'))
        .pipe(plumber())
        .pipe(less())
        .pipe(gulp.dest(destDir.path('stylesheets')));
});

gulp.task('environment', function () {
    var configFile = 'config/env_' + utils.getEnvName() + '.json';
    projectDir.copy(configFile, destDir.path('env.json'), { overwrite: true });
});

gulp.task('watch', function () {
    var beepOnError = function (done) {
        return function (err) {
            if (err) {
                utils.beepSound();
            }
            done(err);
        };
    };

    watch('src/**/*.{js,ts}', batch(function (events, done) {
        gulp.start('bundle', beepOnError(done));
    }));
    watch('src/**/*.less', batch(function (events, done) {
        gulp.start('less', beepOnError(done));
    }));
});

gulp.task('build', ['bundle', 'less', 'environment']);
