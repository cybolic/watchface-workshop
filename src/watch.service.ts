import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

import * as Promise from 'bluebird';

import Watch from './watchmaker/objects/watch';
import * as WatchZip from './watchmaker/loaders/zip';

@Injectable()
export class WatchService {
  public watch : Watch;

  // Observable string sources
  private loadFileSource = new Subject<string>();
  private loadDirSource  = new Subject<string>();
  // Observable string streams
  loadFile$ = this.loadFileSource.asObservable();
  loadDir$  = this.loadDirSource.asObservable();

  loadFile(path: string) {
    this.loadWatchZip(path)
      .then((watch:Watch) => {
        this.watch = watch;
        console.log(this);
        setTimeout(() => {
          this.loadFileSource.next(path);
        }, 1)
      }, (error) => {
         // alert(error);
         throw error;
      })
    ;
  }
  loadDir(path: string) {
    console.log("TODO: Do actual dir loading of: "+path);
    this.loadDirSource.next(path);
  }

  private loadWatchZip(path:string) : Promise.Thenable<Watch> {
    return WatchZip.load(path);
  }
}