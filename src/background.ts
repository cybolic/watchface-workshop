// This is main process of Electron, started as first thing when your
// app starts. This script is running through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

import { app, Menu, ipcMain as ipc } from 'electron';
import { devMenuTemplate } from './menu/dev_menu_template';
import { editMenuTemplate } from './menu/edit_menu_template';
import createWindow from './helpers/window';

import { getSystemFont } from './electron_utils';
const systemFont = getSystemFont();

// Special module holding environment variables which you declared
// in config/env_xxx.json file.
import env from './env';

var mainWindow;

var setApplicationMenu = function () {
    var menus = [editMenuTemplate];
    if (env.name !== 'production') {
        menus.push(devMenuTemplate);
    }
    Menu.setApplicationMenu(Menu.buildFromTemplate(menus));
};

// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== 'production') {
    var userDataPath = app.getPath('userData');
    app.setPath('userData', userDataPath + ' (' + env.name + ')');
}

app.on('ready', function () {
    setApplicationMenu();

    var mainWindow = createWindow('main', {
        width: 1000,
        height: 600,
        darkTheme: true,
        webPreferences: {
            experimentalCanvasFeatures: true,
            blinkFeatures: "CSSVariables2",
            defaultEncoding: 'UTF-8'
        }
    });

    mainWindow.loadURL('file://' + __dirname + '/app.html');


    var propertiesWindow = createWindow('properties', {
        parent: mainWindow,
        width: Math.round(systemFont.size*50), // 640,
        height: Math.round(systemFont.size*27), // 335,
        darkTheme: true,
        show: false
    });
    propertiesWindow.setMenu(null);
    propertiesWindow.on('close', (event) => {
        event.preventDefault();
        propertiesWindow.hide();
        mainWindow.webContents.send('properties-hidden');
    });
    ipc.on('show-properties', () => {
        propertiesWindow.show();
        propertiesWindow.webContents.send('show-properties');
    });
    ipc.on('hide-properties', () => {
        propertiesWindow.hide();
    });
    propertiesWindow.loadURL('file://' + __dirname + '/window-properties.html');
    propertiesWindow.openDevTools({
        mode: 'undocked'
    });

    if (env.name === 'development') {
        mainWindow.openDevTools();
    }
});

app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        if (mainWindow) {
          mainWindow.webContents.closeDevTools()
        }
    }
    app.quit();
});

exports.selectDirectory = function () {
  dialog.showOpenDialog(mainWindow, {
    properties: ['openDirectory']
  });
}

exports.selectFile = function () {
  dialog.showOpenDialog(mainWindow);
}