/// <reference path="../../app/node_modules/phaser/typescript/phaser.d.ts" />

// console.log(EditorLayerPropertyComponent);

import { ipcRenderer as ipc } from 'electron';
import { getWindowByName } from '../electron_utils';

import { Component, Input, Output, EventEmitter, OnDestroy, OnInit, AfterViewInit, NgZone, Pipe } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { WatchService } from '../watch.service';

import { Base as Layer } from '../watchmaker/objects/base';
import * as draw_utils from '../watchmaker/draw_utils';
import * as draw_objects from '../watchmaker/draw_objects';

@Component({
  selector: 'editor',
  // directives: [DropdownDirective, DropdownMenuDirective, DropdownToggleDirective, EditorLayersComponent, EditorLayerPropertyComponent, PropertyEditorComponent],
    // <img id="tester0" style="position: absolute; z-index: 999; top: 0; left: 0; opacity: 0.5; width: auto; height: 33%;">
    // <img id="tester1" style="position: absolute; z-index: 999; top: 33%; left: 0; opacity: 0.5; width: auto; height: 33%;">
    // <img id="tester2" style="position: absolute; z-index: 999; top: 66%; left: 0; opacity: 0.5; width: auto; height: 33%;">
    // <div id="tester2-line" style="position: absolute; z-index: 9999; top: 66%; left: 0; opacity: 0.5; width: 0; height: 16.5%; padding-left: 30%; border-bottom: 1px solid white;"></div>
  templateUrl: 'templates/editor.html',
  host: {
    class: 'c-editor'
  }
})
export class WatchEditorComponent implements OnDestroy, OnInit, AfterViewInit {

  @Input() isUnchanged : boolean;
  @Input() isRoundOverlayVisible : boolean;
  @Input() isPreviewOverlayVisible : boolean;
  @Input() isPropertiesVisible : boolean;
  @Output() isPropertiesVisibleChange = new EventEmitter<boolean>();
  @Input() filepath : string;

  private _subscriptions : Array<any> = [];
  public watchLoaded : boolean = false;
  public layer_numbers : [number];
  public layerSelected : number = -1;
  public layerBeingDragged : number = -1; // index of layer being dragged
  public activeTool : string = 'pointer';
  public dataURLPreviewImage : string | SafeStyle = 'none';
  public game : Phaser.Game;
  public canvasHeight : number = 512;
  public backgroundColor : string = '#000000';
  public rendered : boolean = false;

  private propertiesWindow = getWindowByName('properties');

  constructor (private zone: NgZone, private sanitizer: DomSanitizer, public watchService: WatchService) { }

  ngOnInit () {
    this._subscriptions.push( this.watchService.loadFile$.subscribe( (path:string) => {
      // this.zone.run(() => {
      window['watch'] = this.watchService.watch;
      this.setupRendering();
      this.layer_numbers = Array.apply(null, { length: this.watchService.watch.layers.length }).map(Number.call, Number).reverse();
      if (typeof this.watchService.watch.preview === 'string') {
        this.dataURLPreviewImage = this.sanitizer.bypassSecurityTrustStyle(`url("${this.watchService.watch.preview}")`);
      } else {
        this.dataURLPreviewImage = 'none';
      }
      this.watchLoaded = true;
      // });
      this.propertiesWindow.webContents.send('set-properties', this.watchService.watch.properties);
      console.log("File was loaded: "+path);
    }) );
    this._subscriptions.push( this.watchService.loadDir$.subscribe( (path:string) => {
      console.log("Dir was loaded: "+path);
    }) );
    console.log(this);


    ipc.on('change-watch-property', (emitter, property, value) => {
      this.zone.run(() => {
        this.watchService.watch.properties[property] = value;
      })
    });
    ipc.on('change-watch-background-color', (emitter, color) => {
      this.updateBackgroundColor(color);
    });
    ipc.on('properties-hidden', (emitter) => {
      this.zone.run(() => {
        this.isPropertiesVisible = false;
        this.isPropertiesVisibleChange.emit(this.isPropertiesVisible);
      })
    });
  }

  ngAfterViewInit () {
    console.log(arguments);
    // console.log(this.layer_element);
  }

  setActiveTool (tool : string) {
    this.activeTool = tool;
  }

  onInput ($event) {
    console.log($event);
  }

  onTogglePropertiesVisible () {
    this.isPropertiesVisible = ! this.isPropertiesVisible;
    this.isPropertiesVisibleChange.emit(this.isPropertiesVisible);
  }

  updateBackgroundColor (color? : string) {
    if (color == null) {
      color = this.watchService.watch.properties.bg_color || '#000000';
    }
    if (! color.match(/^#/)) {
      color = '#'+color;
    }
    this.watchService.watch.properties.bg_color = color.split('#')[1];
    this.zone.run(() => {
      this.game.stage.backgroundColor = color;
      this.backgroundColor = color;
    });
  }

  selectLayerByIndex (layer_index) {
    this.layerSelected = layer_index;
  }

  // Triggered from layer object
  onLayerObjectMouseDown (layer_index) {
    this.selectLayerByIndex(layer_index);

    if (this.activeTool == 'move') {
      this.watchService.watch.layers[layer_index].draggingStart = {
        x: this.watchService.watch.layers[layer_index].calculatedProperties.x,
        y: this.watchService.watch.layers[layer_index].calculatedProperties.y
      };
      this.layerBeingDragged = layer_index;
      console.log("Started dragging");
    }
  }

  // Triggered from canvas element
  onCanvasMouseUp ($event) {
    if (this.layerBeingDragged != -1) {
      console.log("Stopped dragging", window.event);
      // console.log(
      //   layer.display.display.x - this.game.world.width*0.5,
      //   layer.display.display.y - this.game.world.height*0.5,
      //   layer.display.display.x, layer.display.display.y,
      //   layer.display.bounds.center.x, layer.display.bounds.center.y
      // );
      // layer.display.dragging = false;
      this.watchService.watch.layers[this.layerBeingDragged].draggingStart = null;
      this.zone.run(() => {
        this.watchService.watch.layers[this.layerBeingDragged].properties.x = this.watchService.watch.layers[this.layerBeingDragged].display.display.x - this.game.world.width*0.5;
        this.watchService.watch.layers[this.layerBeingDragged].properties.y = this.watchService.watch.layers[this.layerBeingDragged].display.display.y - this.game.world.height*0.5;
        this.layerBeingDragged = -1;
      });
    }
  }

  // Triggered from game object
  onCanvasMove (event) {
    if (this.layerBeingDragged != -1) {
      // console.log(
      //   this.layerBeingDragged.display.x - (event.positionDown.x - event.x),
      //   this.layerBeingDragged.display.y - (event.positionDown.y - event.y)
      // );
      // console.log(event.x - this.game.world.width*0.5, event.y - this.game.world.height*0.5, event);
      this.watchService.watch.layers[this.layerBeingDragged].properties.x = this.watchService.watch.layers[this.layerBeingDragged].draggingStart.x - (event.positionDown.x - event.x);
      this.watchService.watch.layers[this.layerBeingDragged].properties.y = this.watchService.watch.layers[this.layerBeingDragged].draggingStart.y - (event.positionDown.y - event.y);
    }
  }

  setupRendering () {
    this.rendered = false;
    if (this.game !== undefined)
      this.game.destroy();
    this.layerSelected = -1;
    // this.game = new Phaser.Game(518, 518, Phaser.AUTO, 'canvas', {create: this.setupWatch, update: this.updateRenderer});
    this.game = new Phaser.Game(this.canvasHeight, this.canvasHeight, Phaser.AUTO, 'canvas', {create: this.setupWatch, update: this.updateRenderer, render: this.render});
  }

  setupWatch = () => {
    let game = this.game;
    this.updateBackgroundColor()

    game.input.addMoveCallback((event) => {
      this.onCanvasMove(event);
    }, this);
    this.watchService.watch.layers.forEach((layer, index) => {

      // if (index != 1) return;
      switch (layer.properties.type) {
        case 'image':
          if (layer.image == null) {
            console.error("Couldn't load layer image:", layer);
            return;
          }
          // layer.source = layer.image;
          // this.createDisplayImageAndMask(layer, layer.image.width, layer.image.height);
          layer.display = new draw_objects.ImageLayer(layer, game);
          break;
        case 'shape':
          layer.display = new draw_objects.ShapeLayer(layer, game);
          break;
        case 'markers':
        case 'markers_hm':
          layer.display = new draw_objects.MarkersLayer(layer, game);
          break;
        case 'text':
          layer.display = new draw_objects.TextLayer(layer, game);
          break;
        case 'text_ring':
          layer.display = new draw_objects.TextRingLayer(layer, game);
          break;
        default:
          console.log("Unsupported layer type:", layer.properties.type, layer.properties);
          break;
      }
      layer.index = index;
      layer.previousProperties = '';
      if (layer.display && layer.display.display) {
        layer.display.display.inputEnabled = true;
        layer.display.display.input.pixelPerfectClick = true;
        layer.display.display.input.pixelPerfectAlpha = 1; // 0 - 255
        // layer.display.display.dragFromCenter = false;
        // layer.display.display.input.enableDrag(false, false, true);
        layer.display.dragging = false;

        layer.display.display.events.onInputDown.add((object) => {
          this.onLayerObjectMouseDown(layer.index);
        });
      }
    });

    // Trigger stage update so bitmapData is usable immediately
    // http://phaser.io/docs/2.6.2/Phaser.BitmapData.html#draw
    game.stage.updateTransform();
  }

  updateRenderer = () => {
    if (this.watchService.watch.layers.length) {
      if (! this.game.time.now || this.game.time.elapsedMS > 16) {
        // if (this.watchService.watch.script != null) {
        this.watchService.watch.script.update();
        this.watchService.watch.layers.forEach((layer, index) => {
          this.updateLayer(layer, index);
        });
        // this.game.debug.inputInfo(16, 16);
        // debugger;
      }
    }
  }

  render = () => {
    if (! this.rendered) {
      this.zone.run(() => {
        console.log("Trigger update on first render.");
      });
    }
    this.rendered = true;
  }

  updateLayer (layer, index) {
    // If this layer isn't drawable, skip it
    if (layer.object == null && layer.display == null)
      return;
    if (this.updateLayerProperties(layer)) {
      layer.display.draw();
    }
    // if (this.updateLayerProperties(layer)) {
    //   if (layer.properties.type == 'text' && ! layer.display) {
    //     // Make sure target bitmap is large enough to show the source
    //     if (layer.rect_scaled && (layer.rect_scaled.width > layer.bitmap.width || layer.rect_scaled.height > layer.bitmap.height)) {
    //       console.log("Resizing target to scaled size");
    //       this.resizeDisplayImageAndMaskSizes(layer, layer.rect_scaled.width, layer.rect_scaled.height);
    //     } else if (layer.source.width > layer.bitmap.width || layer.source.height > layer.bitmap.height) {
    //       console.log("Resizing target");
    //       this.resizeDisplayImageAndMaskSizes(layer, layer.source.width, layer.source.height);
    //     } else if (index == 5) {
    //       console.log(layer.rect_scaled.width, layer.bitmap.width, layer.rect_scaled.height, layer.bitmap.height);
    //     }
    //   }
    //   this.updateLayerDisplay(layer, index, this.game);
    // }
    if (this.layerSelected == index) {
      this.showOutlines(layer);
    }
    layer.previousProperties = JSON.stringify(layer.calculatedProperties); //Object.assign({}, layer.calculatedProperties);
  }

  updateLayerProperties(layer) {
    layer.anchor = layer.getAnchor();
    this.watchService.watch.script.calcLayerProperties(layer);

    let display = layer.object || layer.display;

    display.visible = layer.calculatedProperties.visible;

      // If this layer isn't visible or hasn't hasn't changed since last draw
      // don't update its internals
    if (layer.calculatedProperties.visible == false || JSON.stringify(layer.calculatedProperties) == layer.previousProperties)//JSON.stringify(layer.previousProperties))
      return false;

    if (layer.display) {

      layer.display.update(layer.calculatedProperties);
      return true;

    } else {
      if (layer.properties.type == 'text') {
        // Only continue if the text is actually renderable
        if (layer.calculatedProperties.text !== undefined && layer.calculatedProperties.text != '' && layer.calculatedProperties.text_size != 0) {

          if (layer.calculatedProperties.text_size != layer.previousProperties.text_size) {
            layer.properties.actual_text_size = Math.max(1,
              16 * Math.round(
                (draw_utils.getFontScale(layer.calculatedProperties.font, parseFloat(layer.calculatedProperties.text_size)*1.5) * parseFloat(layer.calculatedProperties.text_size)) / 16));
            layer.source.fontSize = layer.properties.actual_text_size;
          }

          if (layer.calculatedProperties.transform !== '') {
            layer.source.setText(layer.getTransformed(), true);
          } else {
            layer.source.setText(layer.calculatedProperties.text, true);
          }

          // console.log("detecting bounds of", layer.source.context.canvas.width, layer.source.context.canvas.height, layer);
          layer.rect = draw_utils.getCanvasContentBounds(layer.source.context, 0);
          layer.rect_scaled = {
            x: layer.x,
            y: layer.y,
            width: Math.floor(layer.rect.width * (layer.calculatedProperties.text_size / layer.rect.height)),
            height: Math.floor(layer.calculatedProperties.text_size)
          };
        // If the text is not renderable, don't show the layer
        } else {
          display.visible = false;
        }

      }

      if (layer.calculatedProperties.rotation !== undefined)
        display.angle = layer.calculatedProperties.rotation;
      if (layer.calculatedProperties.width !== undefined) {
        display.width = layer.calculatedProperties.width;
      }
      if (layer.calculatedProperties.height !== undefined) {
        display.height = layer.calculatedProperties.height;
      }
      display.tint = parseInt(layer.calculatedProperties.color, 16);
      if (layer.calculatedProperties.shader !== undefined)
        null;
      else if (layer.calculatedProperties.opacity !== undefined)
        display.opacity = layer.calculatedProperties.opacity/100;

      return display.visible;
    }


  }

  updateLayerDisplay(layer, index, game) {
    if (layer.display) {
      console.log("draw");
      layer.display.draw();
      // if (layer.printed != true && index == 2) {
      //   console.log(layer.display.display.worldPosition.x, (layer.display.display.anchor.x * layer.display.bounds.width), layer.display.display.anchor.x, layer.display.bounds.width);
      //   layer.printed = true;
      // }
    } else
    if (layer.bitmap) {
      layer.bitmap.clear();

      this.updateLayerShader(layer);

      // Draw our layer graphics
      layer.x = layer.source.x;
      layer.y = layer.source.y;
      // let x = layer.source.x,// + layer.offset[0],
      //     y = layer.source.y;// + layer.offset[1];
      if (layer.properties.type == 'text') {// && layer.anchor[0] != 0 && layer.anchor[1] != 0) {
        // x = x + (layer.object.width  - layer.source.width)  * layer.anchor[0];
        // y = y + (layer.object.height - layer.source.height) * 0.5;

        // // WatchMaker does vertical alignment a bit differently than canvas ...
        switch (layer.anchor[1]) {
          case 0.0:
            layer.object.pivot.y = layer.source.height * 0.5;
            // y = y - layer.source.height * 0.5;
            break;
          case 0.5:
            layer.object.pivot.y = 0;
            // y = y + (layer.object.height - layer.source.height) * 0.5;
            break;
          case 1.0:
            layer.object.pivot.y = - layer.source.height * 0.5;
            // y = y + layer.source.height * 0.5;
            break;
        }
      }
      if (layer.printed != true && index == 3) {
        // console.log(layer.anchor);
        // console.log(layer.source.x, layer.source.y);
        // console.log(layer.source.width, layer.source.height);
        // console.log(layer.x, layer.y);
        // console.log(
        //   layer.source.x - (layer.anchor[0] * layer.source.width),
        //   layer.source.y - (layer.anchor[1] * layer.source.height)
        // );
        layer.printed = true;
      }


      if (layer.properties.type == 'text') {
        layer.x = layer.x + (layer.object.width  - layer.rect_scaled.width)  * layer.anchor[0];
        layer.y = layer.y + (layer.object.height - layer.rect_scaled.height) * 0.5;
        // console.log("got", layer.rect);
        // layer.bitmap.copy(layer.source, x, y, width, height, tx, ty, newWidth, newHeight, rotate, anchorX, anchorY, scaleX, scaleY, alpha
        layer.bitmap.copy(layer.source,
          layer.rect.x, layer.rect.y,
          layer.rect.width, layer.rect.height,
          layer.x, layer.y,
          layer.rect_scaled.width, layer.rect_scaled.height);
        // console.log("drawn");

      } else {
        layer.bitmap.draw(layer.source, layer.x, layer.y);
      }
      layer.bitmap.blendReset();
    }
  }

  showOutlines (layer) {
    // this.game.debug.spriteBounds(layer.source, 'rgb(0,255,255)', false);
    if (layer.display) {
      this.game.debug.context.lineWidth = 2;
      layer.display.drawDebug();
      // console.log(layer.display.bounds);
      // this.game.debug.geom(layer.display.bounds, 'rgb(0,255,255)', false);

        // new Phaser.Rectangle(
        // layer.object.left,// - layer.object.anchor.x,
        // layer.object.top,// - layer.object.anchor.y,
        // layer.object.width, layer.object.height), 'rgb(0,255,255)', false);

      // this.game.debug.geom(new Phaser.Rectangle(
      //   layer.object.left + (layer.object.width * layer.object.anchor.x),
      //   layer.object.top + (layer.object.height * layer.object.anchor.y) - 5,
      //   1, 10), 'rgb(0,255,255)', false);
      // this.game.debug.geom(new Phaser.Rectangle(
      //   layer.object.left + (layer.object.width * layer.object.anchor.x) - 5,
      //   layer.object.top + (layer.object.height * layer.object.anchor.y),
      //   10, 1), 'rgb(0,255,255)', false);
    } else {
      this.game.debug.geom(new Phaser.Rectangle(
        layer.object.left,// - layer.object.anchor.x,
        layer.object.top,// - layer.object.anchor.y,
        layer.object.width, layer.object.height), 'rgb(0,255,255)', false);

      this.game.debug.geom(new Phaser.Rectangle(
        layer.object.left + (layer.object.width * layer.object.anchor.x),
        layer.object.top + (layer.object.height * layer.object.anchor.y) - 5,
        1, 10), 'rgb(0,255,255)', false);
      this.game.debug.geom(new Phaser.Rectangle(
        layer.object.left + (layer.object.width * layer.object.anchor.x) - 5,
        layer.object.top + (layer.object.height * layer.object.anchor.y),
        10, 1), 'rgb(0,255,255)', false);

      if (layer.properties.type == 'text') {
        this.game.debug.geom(new Phaser.Rectangle(
          layer.x + layer.object.left,// - layer.object.anchor.x,
          layer.y + layer.object.top,// - layer.object.anchor.y,
          layer.rect_scaled.width, layer.rect_scaled.height), 'rgb(255,255,0)', false);
          // this.game.debug.geom(new Phaser.Rectangle(
          //   x + layer.object.left,// - layer.object.anchor.x,
          //   y + layer.object.top,// - layer.object.anchor.y,
          //   layer.source.canvas.width, layer.source.canvas.height), 'rgb(255,255,0)', false);
          this.game.debug.geom(new Phaser.Rectangle(
            layer.source.left + layer.object.left + (layer.object.width * layer.source.anchor.x),
            layer.source.top + layer.object.top + (layer.object.height * layer.source.anchor.y) - 5,
            1, 10), 'rgb(255,255,0)', false);
          this.game.debug.geom(new Phaser.Rectangle(
            layer.source.left + layer.object.left + (layer.object.width * layer.source.anchor.x) - 5,
            layer.source.top + layer.object.top + (layer.object.height * layer.source.anchor.y),
            10, 1), 'rgb(255,255,0)', false);
      }

      this.game.debug.geom(new Phaser.Rectangle(
        layer.object.left + layer.x,
        layer.object.top + layer.y - 5,
        1, 10), 'rgb(255,255,255)', false);
      this.game.debug.geom(new Phaser.Rectangle(
        layer.object.left + layer.x - 5,
        layer.object.top + layer.y,
        10, 1), 'rgb(255,255,255)', false);
    }
  }

  updateLayerShader (layer) {
    if (layer.maskBitmap == null)
      return;

    switch (layer.properties.shader) {
      case 'Segment':
        this.updateMaskSegment(layer.maskBitmap, layer.bitmap,
          layer.calculatedProperties.u_2,            // in color
          layer.calculatedProperties.u_3,            // out color
          parseFloat(layer.calculatedProperties.u_1) // segment angle (degrees)
        );

        // Draw our mask to the shown bitmap and set the composite mode to use the mask
        layer.bitmap.draw(layer.maskBitmap).blendSourceIn();
        break;

      case 'SegmentBetween':
        this.updateMaskSegment(layer.maskBitmap, layer.bitmap,
          layer.calculatedProperties.u_3,            // in color
          layer.calculatedProperties.u_4,            // out color
          parseFloat(layer.calculatedProperties.u_2) - parseFloat(layer.calculatedProperties.u_1) // segment angle (degrees)
        );

        // Draw our mask to the shown bitmap and set the composite mode to use the mask
        this.drawRotatedOnBitmap(layer.bitmap, layer.maskBitmap, parseFloat(layer.calculatedProperties.u_1))
          .blendSourceIn();
        break;

      // args: degree, in color, out color
      case 'Radar':
        let degree = parseFloat(layer.calculatedProperties.u_1),
            in_color  = parseFloat(layer.calculatedProperties.u_2) / 100,
            out_color = parseFloat(layer.calculatedProperties.u_3) / 100;
        // Create the mask background
        layer.maskBitmapBg.clear();
        this.clearBitmapToColor(layer.maskBitmapBg, `rgba(255,255,255,${out_color})`);
        // Create the mask foreground
        this.updateMaskRadar(layer.maskBitmap);
        // Draw our mask background to the shown bitmap
        layer.bitmap.draw(layer.maskBitmapBg);
        // Next, figure out if we need to invert the drawing of our mask foreground
        if (in_color < out_color) {
          layer.bitmap.context.globalCompositeOperation = 'destination-out';
          in_color = 1.0-in_color;
        }
        //
        layer.bitmap.context.globalAlpha = in_color;
        this.drawRotatedOnBitmap(layer.bitmap, layer.maskBitmap, degree, -layer.bitmap.width * 0.25, -layer.bitmap.height * 0.25);
        layer.bitmap.context.globalAlpha = 1.0;
        // Set the composite mode to use the mask
        layer.bitmap.blendSourceIn();
        break;
    }
  }

  createDisplayImageAndMask(layer, width, height) {
    // This is our target canvas to be shown
    layer.bitmap = this.game.make.bitmapData(width, height);

    // Here we generate our mask for shader (mask) rendering
    // We make the mask 1½ times bigger, to allow for rotation without showing the edges
    layer.maskBitmap   = this.game.make.bitmapData(width*1.5, height*1.5);
    layer.maskBitmapBg = this.game.make.bitmapData(width*1.5, height*1.5);

    // And this is the Image object that will show our final image
    layer.object = this.game.add.image(layer.getXCoordinate(this.game.world.width), layer.getYCoordinate(this.game.world.height), layer.bitmap);
    // This is valid, but won't work until TypeScript 2.1
    // layer.object = this.game.add.image(...layer.getXYCoordinates(this.game.world.width, this.game.world.height), layer.bitmap);
    layer.object.anchor.setTo(...layer.getAnchor());
  }

  resizeDisplayImageAndMaskSizes(layer, width, height) {
    layer.bitmap.resize(width, height);
    layer.maskBitmap.resize(width*1.5, height*1.5);
    layer.maskBitmapBg.resize(width*1.5, height*1.5);
  }

  drawRotatedOnBitmap(bitmap, source, degrees, x = 0, y = 0) {
    let Cx = bitmap.width * 0.5,
        Cy = bitmap.height * 0.5;
    bitmap.context.translate(Cx, Cy);
    bitmap.context.rotate(degrees * draw_utils.DEGREES_TO_RADIANS);
    bitmap.context.translate(-Cx, -Cy);
    bitmap.context.drawImage(source.canvas, x, y);
    bitmap.context.resetTransform();
    return bitmap;
  }

  clearBitmapToColor(bitmap, color) {
    bitmap.ctx.beginPath();
    bitmap.ctx.rect(0, 0, bitmap.width, bitmap.height);
    bitmap.ctx.fillStyle = color;
    bitmap.ctx.fill();
  }

  updateMaskSegment(mask, target, in_color, out_color, angle) {
    let draw_id = `segment ${in_color} ${out_color} ${angle}`;
    if (draw_id == mask.lastDraw)
      return;
    mask.clear();
    mask.ctx.beginPath();
    mask.ctx.rect(0, 0, mask.width, mask.height);
    mask.ctx.fillStyle = `rgba(255,255,255,${out_color/100})`;
    mask.ctx.fill();

    if (in_color < out_color) {
      mask.ctx.globalCompositeOperation = 'destination-out';
      mask.ctx.fillStyle = `rgba(255,255,255,${1.0-in_color/100})`;
    } else {
      mask.ctx.globalCompositeOperation = 'source-over';
      mask.ctx.fillStyle = `rgba(255,255,255,${in_color/100})`;
    }
    draw_utils.updatePieMask(mask.ctx,
                             mask.width*.5, mask.height*.5,
                             mask.width,
                             angle);
    mask.ctx.globalCompositeOperation = 'source-over';

    mask.lastDraw = draw_id;
  }

  updateMaskRadar(mask) {
    let draw_id = `radar`;
    if (draw_id == mask.lastDraw)
      return;

    console.log("Updating mask");
    let CX = mask.width / 2,
        CY = mask.height/ 2,
        sx = CX,
        sy = CY,
        length = 100;

    mask.clear();
    mask.ctx.translate(CX, CY);
    mask.ctx.rotate(170 * draw_utils.DEGREES_TO_RADIANS);
    mask.ctx.translate(-CX, -CY);

    for (let i = 0; i < length; i+=0.1) {
        var rad = i * (2*Math.PI) / 360;
        mask.ctx.strokeStyle = `rgba(255,255,255,${i/length})`;
        mask.ctx.beginPath();
        mask.ctx.moveTo(CX, CY);
        mask.ctx.lineTo(CX + sx*2 * Math.cos(rad), CY + sy*2 * Math.sin(rad));
        mask.ctx.stroke();
    }
    mask.ctx.resetTransform();

    mask.lastDraw = draw_id;
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this._subscriptions.forEach( subscription => {
      subscription.unsubscribe();
    });
  }
}