import * as utils from '../electron_utils';
import { ipcRenderer as ipc } from 'electron';

import { DropdownDirective, DropdownMenuDirective, DropdownToggleDirective } from 'ng2-bootstrap';

import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'toolbar',
  // directives: [DropdownDirective, DropdownMenuDirective, DropdownToggleDirective],
  templateUrl: 'templates/toolbar.html',
  host: {
    class: 'c-toolbar'
  }
})
export class ToolbarComponent {

  @Input() isUnchanged: boolean;
  @Input() isRoundOverlayVisible: boolean;
  @Input() isPreviewOverlayVisible: boolean;
  @Input() isPropertiesVisible: boolean;
  @Output() isPropertiesVisibleChange = new EventEmitter<boolean>();
  @Output() onLoadFile = new EventEmitter<string>();
  @Output() onLoadDir  = new EventEmitter<string>();
  @Output() onRoundOverlay  = new EventEmitter<string>();
  @Output() onPreviewOverlay  = new EventEmitter<string>();

  onOpenWatchFile() {
    utils.showOpenFileDialog({
      title: "Select a watch file to load",
      filters: [
        {name: 'Watch files', extensions: ['watch', 'zip']},
        {name: 'All Files', extensions: ['*']}
      ]
    }).then((filenames) => {
      if (filenames) {
        this.onLoadFile.emit(filenames[0]);
      }
    });
  }
  onOpenWatchFolder() {
    utils.showOpenDirectoryDialog({
      title: "Select a watch directory to load"
    })
    .then((filenames) => {
      if (filenames) {
        this.onLoadDir.emit(filenames[0]);
      }
    });
  }
  onToggleRoundOverlay() {
    this.onRoundOverlay.emit();
  }
  onTogglePreviewOverlay() {
    this.onPreviewOverlay.emit();
  }
  onToggleProperties() {
    this.isPropertiesVisible = ! this.isPropertiesVisible;
    this.isPropertiesVisibleChange.emit(this.isPropertiesVisible);
    if (this.isPropertiesVisible) {
      ipc.send('show-properties');
    } else {
      ipc.send('hide-properties');
    }
  }
}