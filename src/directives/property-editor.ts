import { Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: '[property-editor]',
  templateUrl: 'templates/property-editor.html',
  host:  {
    'class' : 'c-property-editor',
    '[class.c-property-editor_half-size]' : 'half_size'
  }
})
export class PropertyEditorComponent implements OnChanges {
  @Input() name: string;
  @Input() text: string;
  @Input() type: string;
  @Input() min: number;
  @Input() max: number;
  @Input() step: number = 1;
  @Input() options: [string];
  @Input() half_size: boolean;
  @Input() model: any;
  @Input() calculatedModel: any;
  @Input() scriptable: boolean = true;
  @Output() modelChange = new EventEmitter();
  @Input() tooltip: string;

  id : string;
  value : any;
  range_value : number;

  public isScript : boolean = false;
  private isCenteredRange : boolean = false;
  private _last_update_time : number = 0;
  private mouse_down : boolean = false;

  constructor (private element: ElementRef) {}

  toggleIsScript () {
    this.isScript = ! this.isScript;
    this.forcedUpdate();
  }

  ngOnChanges (changes: SimpleChanges) {
    if (changes['name']) {
      this.id = 'propertyEdit'+changes['name'].currentValue.replace(/^([a-z])/, (match) => {return match.toUpperCase();});
    }
    // Get the current value from the model, if needed
    if (this.model[this.name] != this.value) {
      this.setValue(this.model[this.name]);
    }
    if (changes['min']) {
      this.min = parseInt(String(changes['min'].currentValue));
    }
    if (changes['max']) {
      this.max = parseInt(String(changes['max'].currentValue));
    }
    if (changes['step']) {
      this.step = parseInt(String(changes['step'].currentValue)) || 1;
    }
    if (this.type == 'range') {
      if (changes['min'] || changes['max']) {
        this.isCenteredRange = Math.abs(this.min) == this.max;
      }
      // Angular doesn't seem to be sending us updates, so we're using a direct access method in this.forcedUpdate instead
      // if (changes['calculatedModel'] && this.isScript) {
        // console.log("calculatedModel:", changes['calculatedModel'].currentValue[this.name]);

      if (changes['min'] || changes['max']) {
        this.updateRangeStyle();
      }
    }
  }

  setValue (value) {
    this.value = value;
    this.isScript = isNaN(this.value);
    if (this.type == 'range') {
      if (this.isScript) {
        this.forcedUpdate();
      } else {
        this.range_value = parseInt(String(this.value)) || 0;
      }
    }
    this.updateWidgets();
  }

  onModelChange ($event) {
     // Value updated from internal element
    this.setValue($event.target.value);
    this.modelChange.emit(this.value);
  }

  forcedUpdate (timestamp? : number) {
    // Only update on first run or around 15FPS
    if (timestamp == null || timestamp - this._last_update_time > 66) {
      if (this.isScript) {
        this.range_value = parseInt(String(this.calculatedModel[this.name])) || 0;
        this.updateRangeStyle();
        window.requestAnimationFrame(this.forcedUpdate.bind(this));
      }
    } else {
      this._last_update_time = timestamp;
      window.requestAnimationFrame(this.forcedUpdate.bind(this));
    }
  }

  updateWidgets () {
    if (this.type == 'color') {
      this.model[this.name] = this.value.split('#')[1];
    } else {
      this.model[this.name] = this.value;
    }
    if (this.type == 'range') {
      this.updateRangeStyle();
    }
  }

  onDoubleclick ($event) {
    if (!this.isScript && this.type == 'range') {
      this.setValue(0);
      $event.preventDefault();
    }
  }

  onWheel ($event) {
    if (!this.isScript) {
      if ($event.wheelDeltaY < 0) {
        if ($event.target.type && $event.target.type != 'range') {
          this.setValue(Math.max(this.min, this.range_value - 1));
        } else {
          this.setValue(Math.max(this.min, this.range_value - this.step));
        }
      } else {
        if ($event.target.type && $event.target.type != 'range') {
          this.setValue(Math.min(this.max, this.range_value + 1));
        } else {
          this.setValue(Math.min(this.max, this.range_value + this.step));
        }
      }
      $event.preventDefault();
    }
  }

  onRangeWrapperClick ($event) {
    if ($event.buttons == 1) {
      this.mouse_down = true;
      this.onRangeWrapperMove($event);
    } else {
      this.mouse_down = false;
    }
  }
  onRangeWrapperMove ($event) {
    // For some reason, we get click events from outside the actual element's size
    // so narrow down what we handle
    if (this.mouse_down && $event.offsetX >= 0 && $event.offsetX <= $event.target.offsetWidth-2) {
      this.setValue(Math.round(($event.offsetX / ($event.target.offsetWidth-2)) * (Math.abs(this.min) + this.max) + this.min));
    }
  }

  updateRangeStyle () {
    if (this.isCenteredRange) {
      if (this.range_value < 0) {
        this.element.nativeElement.style.setProperty('--start-value', (50-((this.range_value/this.min) * 50))+'%');
        this.element.nativeElement.style.setProperty('--end-value', '50%');
      } else {
        this.element.nativeElement.style.setProperty('--start-value', '50%');
        this.element.nativeElement.style.setProperty('--end-value', (50+((this.range_value/this.max) * 50))+'%');
      }
    } else {
      this.element.nativeElement.style.setProperty('--start-value', '0%');
      this.element.nativeElement.style.setProperty('--end-value', (((this.range_value-this.min)/(this.max-this.min))*100)+'%');
    }
  }
}