import { Component, Input, HostListener } from '@angular/core';

import { WatchService } from '../watch.service';

@Component({
  selector: 'ui-splitview',
  template: `
    <ng-content select="[ui-splitview-left]" class="o-splitview__left"></ng-content>
    <div class="o-splitview__handle"
      (mousedown)="onMouseDown($event)"
    ></div>
    <ng-content select="[ui-splitview-right]" class="o-splitview__right"></ng-content>
  `,
  host: {
    class: 'o-splitview'
  }
})
export class UISplitViewComponent {
  private resizing : boolean = false;
  private handle : any = null
  private click_offset : number = 0;

  @Input() right_min_size : number = 0;

  left_max_size = 100;

  onMouseDown ($event) {
    this.resizing = true;
    this.handle = $event.target;
    this.click_offset = $event.offsetX;
    this.left_max_size = 100 - this.right_min_size;
  }

  @HostListener('mousemove', ['$event'])
  onMouseMove ($event) {
    if (this.resizing) {
      let area = this.handle.previousElementSibling.offsetWidth +
                 this.handle.nextElementSibling.offsetWidth +
                 this.handle.offsetWidth;
      let x = $event.x - this.click_offset - this.handle.previousElementSibling.offsetLeft;
      this.handle.previousElementSibling.style.setProperty(
        // 'flex-basis', Math.min(Math.max(0, ((x - area*0.5) / (area*0.5)) * 100), 80)+'%'
        'flex-basis', Math.min(Math.max(0, ((x - area*0.5) / (area*0.5)) * 100), this.left_max_size)+'%'
      );
    }
  }

  @HostListener('mouseup', ['$event'])
  onMouseUp ($event) {
    this.resizing = false;
  }
}