import { Component, Input, Output, EventEmitter, ElementRef, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';

import { WatchService } from '../watch.service';

@Component({
  selector: '[editor-layer]',
  templateUrl: 'templates/editor-layer.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditorLayerComponent implements OnChanges {
  // @Input() layer: any;
  @Input() index: number;
  @Input() selected: boolean;
  @Output() selectedChange = new EventEmitter<boolean>();
  @Input() visible: boolean;
  @Output() visibleChange = new EventEmitter<boolean>();
  @Input() onlyVisible : boolean = false;
  @Output() onlyVisibleChange = new EventEmitter<boolean>();
  // visible : boolean = true;

  constructor (private element: ElementRef, private watchService: WatchService) {
  }

  ngOnChanges (changes: SimpleChanges) {
    // If this layer is selected, scroll to make sure it's visible
    if (changes['selected'] && changes['selected'].currentValue == true) {
      this.scrollIntoView();
    }
    // if (changes['visible'] && changes['visible'].currentValue != this.layer.properties.visible) {
    //     this.layer.properties.visible = changes['visible'].currentValue;
    // }
    if (changes['visible'] && changes['visible'].currentValue != this.watchService.watch.layers[this.index].properties.visible) {
      this.watchService.watch.layers[this.index].properties.visible = changes['visible'].currentValue;
    }
  }

  onSelect ($event) {
    if ((<any>$event).ctrlKey) {
      this.selected = false;
    } else {
      this.selected = true;
    }
    this.selectedChange.emit(this.selected);
    console.log(this.watchService.watch.layers[this.index])
  }

  onToggleVisibility ($event) {
    if ($event.shiftKey) {
      this.visible = true;
      this.onlyVisible = ! this.onlyVisible;
      this.onlyVisibleChange.emit(this.onlyVisible);
    } else {
      this.visible = ! this.visible;
      this.visibleChange.emit(this.visible);
    }
    // this.layer.properties.visible = this.visible;
    this.watchService.watch.layers[this.index].properties.visible = this.visible;
  }

  scrollIntoView () {
    this.element.nativeElement.scrollIntoViewIfNeeded();
  }

  getLayerType () {
    return this.watchService.watch.layers[this.index].properties.type;
  }

  getLayerName () {
    // console.log("Get layer name for", this.index);
    if (this.watchService.watch.layers[this.index].properties.name) {
      return this.watchService.watch.layers[this.index].properties.name;
    } else if (this.watchService.watch.layers[this.index].properties.path) {
      return this.watchService.watch.layers[this.index].properties.path.replace(/^images\//, '');
    } else {
      return `Layer ${this.index + 1}`;
    }
  }
  getLayerThumb () {
    // console.log("Get layer thumb for", this.index);
    if (this.watchService.watch.layers[this.index].image) {
      return 'url('+this.watchService.watch.layers[this.index].image.src+')';
    } else if (this.watchService.watch.layers[this.index].display && this.watchService.watch.layers[this.index].display.bitmap.canvas) {
      return 'url('+this.watchService.watch.layers[this.index].display.bitmap.canvas.toDataURL()+')';
    } else {
      return 'none';
    }
  }
}