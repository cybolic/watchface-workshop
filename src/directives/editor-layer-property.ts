import { Component, Input, OnInit, ElementRef, Renderer, Inject } from '@angular/core';

import { WatchService } from '../watch.service';

@Component({
  selector: '[editor-layer-property]',
  template: `
    <label [attr.for]="id" class="control-label" [ngClass]="{'col-sm-2': !half_size, 'col-sm-4': half_size}">{{text}}:</label>
    <div [ngClass]="{'col-sm-10': !half_size, 'col-sm-8': half_size}" [class.c-input-range]="type == 'range'"
      tooltip="{{watchService.watch.layers[layerIndex].calculatedProperties[name]}}">
      <ng-container *ngIf="type != 'select'">
        <input [id]="id" class="form-control" [class.c-input-range__range]="type == 'range'"
          [type]="type"
          [min]="min" [max]="max" [step]="step"
          [(ngModel)]="watchService.watch.layers[layerIndex].properties[name]">
        <input *ngIf="type == 'range'"
          [id]="id+'Value'" class="c-input-range__value" type="number"
          [(ngModel)]="watchService.watch.layers[layerIndex].properties[name]">
      </ng-container>
      <ng-container *ngIf="type == 'select'">
        <select [id]="id" class="form-control c-input-range__select"
          [(ngModel)]="watchService.watch.layers[layerIndex].properties[name]">
          <option *ngFor="let option of options" [value]="option">{{option}}</option>
        </select>
      </ng-container>
    </div>
  `
})
export class EditorLayerPropertyComponent implements OnInit {
  id : string;
  @Input() name: string;
  @Input() text: string;
  @Input() type: string;
  @Input() min: number;
  @Input() max: number;
  @Input() step: number;
  @Input() options: [string];
  @Input() half_size: boolean;
  @Input() layerIndex: number;

  constructor (@Inject(WatchService) public watchService: WatchService, private element: ElementRef, private renderer: Renderer) {
    renderer.setElementClass(element.nativeElement, 'form-group', true);
  }

  ngOnInit () {
    this.id = 'inputWatch'+name;
    console.log(this.half_size);
    if (this.half_size) {
      this.renderer.setElementClass(this.element.nativeElement, 'col-sm-6', true);
    }
  }
}