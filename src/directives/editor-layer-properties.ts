import { Component, Input } from '@angular/core';

import { WatchService } from '../watch.service';

@Component({
  selector: 'editor-layer-properties',
  templateUrl: 'templates/editor-layer-properties.html'
})
export class EditorLayerPropertiesComponent {
  @Input() layer: any;

  constructor () {}
}