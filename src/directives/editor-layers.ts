import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';

@Component({
  selector: 'editor-layers',
  templateUrl: 'templates/editor-layers.html'
})
export class EditorLayersComponent implements OnInit {
  @Input() selectedLayerIndex: number;
  @Output() selectedLayerIndexChange = new EventEmitter<number>();
  @Input() layerIndexes: Array<number>;
  visibleIndexes : Array<number>;
  onlyVisibleIndex : number = -1;

  constructor (private zone : NgZone, private changeRef: ChangeDetectorRef) {}

  ngOnInit () {
    this.visibleIndexes = this.layerIndexes.slice();
  }

  onSelectionChange (layer_index, state) {
    if (state == true) {
      this.selectedLayerIndex = layer_index;
    }
    this.selectedLayerIndexChange.emit(this.selectedLayerIndex);
  }

  getLayerVisible (layer_index) {
    return this.visibleIndexes.indexOf(layer_index) !== -1;
  }

  onOnlyVisibleChange (layer_index, state) {
    if (state) {
      this.onlyVisibleIndex = layer_index;
      this.visibleIndexes = [layer_index];
    } else {
      this.onlyVisibleIndex = -1;
      this.visibleIndexes = this.layerIndexes.slice();
    }
    this.changeRef.detectChanges();
  }
}