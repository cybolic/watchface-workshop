export default {
  /* Date Tags */

  'dd': 9, // Day in month
  'ddz': '09', // Day in month (with leading zero)
  'ddy': 282, // Day in year
  'ddw2': 'Th', // Day of week
  'ddw': 'Thu', // Day of week
  'ddww': 'Thursday', // Day of week
  'ddw0': 5, // Day of week (Sun = 0, Sat = 6)
  'ddim': 31, // Days in current month
  'dn': 1, // Month in year
  'dnn': '01', // Month in year
  'dnnn': 'Jan', // Month in year
  'dnnnn': 'January', // Month in year
  'dy': 14, // Year (2 digits)
  'dyy': 2014, // Year (4 digits)
  'dwm': 4, // Week in month
  'dw': 40, // Week in year


  /* Time Tags */

  'dh': 9, // Hour in day (1-12)
  'dh11': 21, // Hour in day (0-11)
  'dh24': 9, // Hour in day (1-24)
  'dh23': 21, // Hour in day (0-23)
  'dht': 'nine', // Hour in day text (1-12)
  'dh24t': 'twenty-one', // Hour in day text (1-24)
  'dhz': '09', // Hour in day (1-12) (with leading zero)
  'dh11z': '21', // Hour in day (0-11) (with leading zero)
  'dh24z': '09', // Hour in day (1-24) (with leading zero)
  'dh23z': '21', // Hour in day (0-23) (with leading zero)
  'dm': 7, // Minute in hour
  'dmz': '07', // Minute in hour (with leading zero)
  'dhtt': 0, // Hour in day (1-12 tens)
  'dhto': 9, // Hour in day (1-12 ones)
  'dh11tt': 0, // Hour in day (0-11 tens)
  'dh11to': 9, // Hour in day (0-11 ones)
  'dh24tt': 0, // Hour in day (1-24 tens)
  'dh24to': 9, // Hour in day (1-24 ones)
  'dh23tt': 0, // Hour in day (0-23 tens)
  'dh23to': 9, // Hour in day (0-23 ones)
  'dmt': 3, // Minute in hour (tens)
  'dmo': 1, // Minute in hour (ones)
  'dmat': 'thirty one', // Minute in hour text (all)
  'dmtt': 'thirty', // Minute in hour text (tens)
  'dmot': 'one', // Minute in hour text (ones)
  'ds': 2, // Second in minute
  'dsz': '02', // Second in minute (with leading zero)
  'da': 'AM', // AM/PM
  'dss': 32, // Milliseconds
  'dssz': '032', // Milliseconds (with leading zeros)
  'dsps': 1207, // Seconds * 1000 + milliseconds
  'dz': 'BST', // Timezone
  'dtp': 0.74, // Time (% 24 hours)
  'drh': 290, // Rotation value for hour (12h, adj for mins)
  'drh24': 145, // Rotation value for hour hand (24h, adj for mins)
  'drh0': 270, // Rotation value for hour hand (12h)
  'drm': 242, // Rotation value for min hand (adj for secs)
  'drs': 156, // Rotation value for second hand
  'drss': 156.2, // Rotation smooth value for second hand
  'drms': 193.4, // Rotation value for milliseconds


  /* Counter Tags */

  'c_elapsed': 50, // Seconds elapsed since loaded
  'c_0_100_2_st': 50, // 0 to 100 in 2s, then stop
  'c_0_100_2_rp': 50, // 0 to 100 in 2s, then repeat
  'c_0_100_2_rv': 50, // 0 to 100 in 2s, then reverse
  'c_0_100_2_rv_2': 50, // As above with 2s start delay


  /* Time Zone Tags */

  'tz1l': 'Los Angeles', // Extra Time Zone 1 Location
  'tz1o': '00', // Extra Time Zone 1 Offset  - 8:
  'tz1t': 26, // Extra Time Zone 1 Time  01:
  'tz1rh': 42, // Extra Time Zone 1 Rotation hour hand
  'tz1rh24': 24, // Extra Time Zone 1 Rotation hour hand (24h)
  'tz1rm': 160, // Extra Time Zone 1 Rotation minute hand
  'tz2l': 'London', // Extra Time Zone 2 Location
  'tz2o': '00', // Extra Time Zone 2 Offset  + 0:
  'tz2t': 26, // Extra Time Zone 2 Time  09:
  'tz2rh': 283, // Extra Time Zone 2 Rotation hour hand
  'tz2rh24': 141, // Extra Time Zone 2 Rotation hour hand (24h)
  'tz2rm': 160, // Extra Time Zone 2 Rotation minute hand
  'tz3l': 'Tokyo', // Extra Time Zone 3 Location
  'tz3o': '00', // Extra Time Zone 3 Offset  + 9:
  'tz3t': 26, // Extra Time Zone 3 Time  18:
  'tz3rh': 190, // Extra Time Zone 3 Rotation hour hand
  'tz3rh24': 95, // Extra Time Zone 3 Rotation hour hand (24h)
  'tz3rm': 160, // Extra Time Zone 3 Rotation minute hand

  /* Battery Tags */

  'bl': 71, // Battery level
  'blp': '71%', // Battery level %
  'br': 250, // Rotation value for Battery level
  'btc': 27, // Battery temperature (C)
  'btf': 81, // Battery temperature (F)
  'btcd': '27°C', // Battery temperature (C) (percent)
  'btfd': '81°F', // Battery temperature (F) (percent)
  'bc': 'Charging', // Battery charging status


  /* Phone Tags */

  'pbl': 71, // Battery Phone level
  'pblp': '71%', // Battery Phone level %
  'pbr': 250, // Rotation value for Battery Phone level
  'pbtc': 27, // Battery Phone temperature (C)
  'pbtf': 81, // Battery Phone temperature (F)
  'pbtcd': '27°C', // Battery Phone temperature (C) (percent)
  'pbtfd': '81°F', // Battery Phone temperature (F) (percent)
  'pbc': 'Charging', // Battery Phone charging status
  'pws': 0.7, // Phone Wifi Strength %
  'pwc': 'Y', // Phone Wifi Connected Y/N


  /* Device Tags */

  'aname': 'GT1505', // Device name
  'aman': 'samsung', // Device manufacturer
  'awname': 'Motorola 360', // Watch name
  'around': true, // Is round?
  'atyre': true, // Has flat tyre?
  'abright': true, // Is bright?
  'abss': 150, // Milliseconds since bright (-1 for dim)
  'alat': 40.7127, // Current latitude
  'alon': 74.0059, // Current longitude
  'alatd': '40.7127°', // Current latitude (degrees)
  'alond': '74.0059°', // Current longitude (degrees)
  'alatdd': '40.7127°N', // Current latitude (degrees + direction)
  'alondd': '74.0059°E', // Current longitude (degrees + direction)


  /* Stopwatch Tags */

  'swh': 0, // Stopwatch hours
  'swm': 1, // Stopwatch minutes
  'sws': 25, // Stopwatch seconds
  'swss': 274, // Stopwatch milliseconds (2 digits)
  'swsss': 27, // Stopwatch milliseconds (3 digits)
  'swsst': 1274, // Stopwatch milliseconds total
  'swr': true, // Stopwatch is running?
  'swrm': 30, // Stopwatch minute rotation
  'swrs': 45, // Stopwatch second rotation
  'swrss': 47.2, // Stopwatch millisecond rotation


  /* Weather Tags */

  'wl': 'London', // Weather Location
  'wt': 13, // Current Temperature
  'wth': 14, // Today's High
  'wtl': 12, // Today's Low
  'wtd': '13°C', // Current Temperature (degrees)
  'wthd': '14°C', // Today's High (degrees)
  'wtld': '12°C', // Today's Low (degrees)
  'wm': 'c', // Weather Units
  'wct': 'Rain', // Current Condition Text
  'wci': '10d', // Current Condition Icon
  'wh': 87, // Current Humidity Number
  'whp': '87%', // Current Humidity Percentage
  'wp': 1003, // Atmospheric Pressure
  'wws': 6.2, // Wind Speed (mph)
  'wwd': 140, // Wind Direction (degrees)
  'wwdb': 'NE', // Wind Direction (NE)
  'wwdbb': 'NNE', // Wind Direction (NNE)
  'wcl': 0.4, // Cloudiness (%)
  'wr': 30, // Rain volume for last 3 hrs (mm)
  'wsr': 14, // Sunrise time  08:
  'wss': 15, // Sunset time 17:
  'wsrp': 0.3, // Sunrise time (% 24 hours)
  'wssp': 0.7, // Sunset time (% 24 hours)
  'wmp': 3, // Moon Phase (1=young, 5=full, 9=old)
  'wml': 'York', // Weather manual location New
  'wlu': 27, // Weather last update 12:08:
  'wf0dt': 11, // Forecast Day 0 Temp
  'wf0dth': 12, // Forecast Day 0 High
  'wf0dtl': 5, // Forecast Day 0 Low
  'wf0dct': 'Rain', // Forecast Day 0 Condition Text
  'wf0dci': '10d', // Forecast Day 0 Condition Icon
  'wf1dt': 11, // Forecast Day 1 Temp
  'wf1dth': 12, // Forecast Day 1 High
  'wf1dtl': 5, // Forecast Day 1 Low
  'wf1dct': 'Rain', // Forecast Day 1 Condition Text
  'wf1dci': '10d', // Forecast Day 1 Condition Icon

  /* Calendar Tags */

  'cex': true, // Events Exist?
  'c1t': 'Gym', // Event 1 Text
  'c1ex': true, // Event 1 Exists?
  'c1b': '00', // Event 1 Begin 19:
  'c1br': 210, // Event 1 Begin Rotation (12 hours)
  'c1bp': 0.79, // Event 1 Begin % 24 Hours
  'c1e': '00', // Event 1 End 20:
  'c1er': 240, // Event 1 End Rotation (12 hours)
  'c1ep': 0.83, // Event 1 End % 24 Hours
  'c1l': 'Statue of Liberty', // Event 1 Location
  'c1c': 'ff0000', // Event 1 Color
  'c1i': 520, // Event 1 ID

  /* Sensor Tags */

  'ssc': 1046, // Step Count
  'shr': 72, // Heart Rate
  'sax': 0.5, // Accelerometer X
  'say': 0.5, // Accelerometer Y
  'saz': 0.5, // Accelerometer Z
  'sgx': 0.5, // Gyroscope X
  'sgy': 0.5, // Gyroscope Y
  'sgz': 0.5, // Gyroscope Z
  'scr': -210, // Compass for Rotation (needs negative)
  'sct': 210, // Compass Display (0=N, 90=E, 180=S, 270=W)
  'sctd': '210°', // Compass Display (degrees)
  'scb': 'SW', // Compass Bearing (NE)
  'scbb': 'SSW', // Compass Bearing (NNE)
  'sctdb': '210° SW', // Compass Display (degrees + NE bearing)
  'sctdbb': '210° SSW', // Compass Display (degrees + NNE bearing)
}