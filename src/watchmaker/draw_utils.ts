import * as Promise from 'bluebird';

export const DEGREES_TO_RADIANS = Math.PI/180;
export const RADIANS_TO_DEGREES = 180/Math.PI;
export const TWO_PI = Math.PI * 2;

export const get_coords_from_alignment = function (alignment, width, height) {
  let coords = [0, 0];
  switch (alignment[1]) {
    case 'l':
      coords[0] = 0;
      break;
    case 'c':
      coords[0] = -width * 0.5;
      break;
    case 'r':
      coords[0] = -width;
      break;
  }
  switch (alignment[0]) {
    case 't':
      coords[1] = 0;
      break;
    case 'c':
      coords[1] = -height * 0.5;
      break;
    case 'b':
      coords[1] = -height;
      break;
  }
  return coords;
}

export const rgb_to_hsv = function (r, g, b) {
  let h, s, v = 0;

  let rgb_min = Math.min(r, g, b);
  let rgb_max = Math.max(r, g, b);

  v = rgb_max;
  if (v === 0) {
    h = 0;
    s = 0;
  } else {
    s = 266 * (rgb_max - rgb_min) / v;
    if (s === 0) {
      h = 0;
    } else {
      if (rgb_max === r) {
        h = 0 + 43 * (g - b) / (rgb_max - rgb_min);
      } else if (rgb_max === g) {
        h = 85 + 43 * (b - r) / (rgb_max - rgb_min);
      } else {
        h = 171 + 43 * (r - g) / (rgb_max - rgb_min);
      }
    }
  }
  return [h, s, v];
}

const createPoint = function (x, y) { return {x:x,y:y}; };
const rotateXY = function (x, y, angle) {
  let rad = angle * DEGREES_TO_RADIANS;
  let cosVal = Math.cos(rad);
  let sinVal = Math.sin(rad);
  return createPoint(cosVal*x - sinVal*y,
                     sinVal*x + cosVal*y);
};

export const computePieMaskPolygon = function (x, y, radius, angle) {
    while (angle < 0)
      angle += 360;
    angle %= 360;

    let delta = rotateXY(0, -2*radius, angle);
    let pts = [createPoint(x,y-2*radius),
               createPoint(x,y),
               createPoint(x+delta.x, y+delta.y)];

    if (angle > 270)
        pts.push(createPoint(x-2*radius,y));
    if (angle > 180)
        pts.push(createPoint(x,y+2*radius));
    if (angle > 90)
        pts.push(createPoint(x+2*radius,y));

    return pts;
};

export const updatePieMask = function (mask, x, y, radius, angle) {
  let pts = computePieMaskPolygon(x, y, radius, angle);
  mask.beginPath();
  mask.moveTo(pts[0].x, pts[0].y);
  for (let i = 1; i < pts.length; ++i) {
    mask.lineTo(pts[i].x, pts[i].y);
  }
  mask.lineTo(pts[0].x, pts[0].y);
  mask.closePath();
  mask.fill();
};

export const getCanvasContentBounds = function (context, min_alpha = 0, stop_on_first_blank_row = false) {
  let width = context.canvas.width,
      height = context.canvas.height,
      startX = -1,
      startY = -1,
      endX = -1,
      endY = -1,
      row_is_blank = true,
      pixels = context.getImageData(0, 0, width, height).data;

  for (let y = 0; y < height; y++) {
    row_is_blank = true;
    for (let x = 0; x < width; x++) {
      if (pixels[((y * width + x) * 4)+3] > min_alpha) {
        row_is_blank = false;
        if (startY === -1) {
          startX = x;
          startY = y;
        } else {
          if (x < startX) {
            startX = x;
          } else if (x > endX) {
            endX = x;
          }
          endY = y;
        }
      }
    }
    if (stop_on_first_blank_row && row_is_blank && endX !== -1) {
      break;
    }
  }
  return {
    x: startX,
    y: startY,
    width: endX - startX,
    height: endY - startY
  };
}

var fontLoadedCache = {};
export const waitForFontLoad = function (font : string) : Promise.Thenable<void> {
  if (fontLoadedCache[font] == true) {
    return Promise.resolve();
  }

  return new Promise((resolve, reject) => {
    let retries = 5;
    // Create the initial comparison image data
    let canvas = document.createElement('canvas');
    canvas.width = 20;
    canvas.height = 20;
    let ctx = canvas.getContext('2d');

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = 'white';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';
    ctx.font = `14px sans`;
    ctx.fillText('M9', 0, 0);
    let imagedata_unloaded = ctx.getImageData(0, 0, canvas.width, canvas.height).data.toString();

    let check_if_loaded = function () {
      retries -= 1;

      ctx.fillStyle = 'black';
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      ctx.fillStyle = 'white';
      ctx.textAlign = 'left';
      ctx.textBaseline = 'top';
      ctx.font = `14px ${font}`;
      ctx.fillText('M9', 0, 0);

      // If the new image data is the same as the unloaded data
      if (imagedata_unloaded == ctx.getImageData(0, 0, canvas.width, canvas.height).data.toString()) {
        if (retries > 0) {
          // console.log("Bitmap data hasn't changed, font is still not loaded:", font);
          setTimeout( check_if_loaded, 200 );
        } else {
          reject();
        }
      } else {
        // console.log("Bitmap data changed, font is loaded:", font);
        fontLoadedCache[font] = true;
        resolve();
      }
    }
    check_if_loaded();
  });
}

// export const determineFontHeight = function (fontStyle) {
  // let body = document.getElementsByTagName('body')[0],
  //     dummy = document.createElement('div'),
  //     dummyText = document.createTextNode('M'),
  //     height = 0;
  // dummy.appendChild(dummyText);
  // dummy.setAttribute('style', 'position:absolute; top:0; left:0; z-index:-1; '+fontStyle);
  // body.appendChild(dummy);
  // height = dummy.offsetHeight;
  // body.removeChild(dummy);
  // return parseFloat(height);
var fontHeightCache = {};
export const determineFontHeightOld = function (font, size) {
  let result = fontHeightCache[`${font}-${size}`];
  if (!result) {
    let canvas = document.createElement('canvas');
    canvas.width = size*2;
    canvas.height = size*2;
    let ctx = canvas.getContext('2d'),
        metrics = null;

    ctx.fillStyle = 'black';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';
    ctx.font = `${size}px ${font}`;
    ctx.fillText('M', 0, 0);
    metrics = ctx.measureText('M');
    // console.log(ctx.font, metrics);
    // return ctx.measureText('M').ideographicBaseline;
    // return (size - metrics.fontBoundingBoxAscent) + metrics.fontBoundingBoxDescent;
    result = Math.floor((<any>metrics).hangingBaseline * 2);
    fontHeightCache[`${font}-${size}`] = result;
  }
  return result;
};
// sample_text = 'gM' would produce more accurate results, but wouldn't match WatchMaker
export const determineFontHeight = function (font, size, sample_text='M80179') {
  let result = fontHeightCache[`${font}-${size}`];
  if (!result) {
    let canvas = document.createElement('canvas');
    canvas.width = size*4;
    canvas.height = size*2;
    let ctx = canvas.getContext('2d');

    // ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = 'white';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';
    ctx.font = `${size}px ${font}`;
    ctx.fillText(sample_text, 0, 0);
    // console.log(ctx);    var pixels = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
    // var start = -1;
    // var end = -1;
    // var row_is_blank = true;
    // for (var row = 0; row < canvas.height; row++) {
    //   row_is_blank = true;
    //   for (var column = 0; column < canvas.width; column++) {
    //     var index = (row * canvas.width + column) * 4;
    //     if (pixels[index] < 60) {
    //       if (row_is_blank && column === canvas.width - 1 && start !== -1) {
    //         end = row;
    //         row = canvas.height;
    //         break;
    //       }
    //       continue;
    //     } else {
    //       row_is_blank = false;
    //       if (start === -1) {
    //         start = row;
    //       }
    //       break;
    //     }
    //   }
    // }
    // result = end - start;
    result = getCanvasContentBounds(ctx, 127, true).height;

    fontHeightCache[`${font}-${size}`] = result;
    // console.log(font, "Asked for font height:", size, "got height:", result, "ratio is:", size / result);
    // document.body.appendChild(canvas);
  }
  return result;
};

export const getFontScale = function (font, size = 100) {
  // We add 4 to the height to get around top padding, which we can't seem to read
  let height = determineFontHeight(font, size);
  return size / height;
}

export const approximateFontSize = function (font, size, attempts = 5) {
  let target_font_size = parseFloat(size),
      current_font_size = parseFloat(size),
      rendered_font_size = determineFontHeight(font, target_font_size),
      results = [];
  ;
  while (attempts > 0 && Math.round(rendered_font_size) != Math.round(target_font_size)) {
    // console.log("aiming for",target_font_size, "currently at", current_font_size, "scaling by", (target_font_size / rendered_font_size));
    attempts -= 1;
    if (results.length > 2 && rendered_font_size == results[results.length-3][1] && rendered_font_size != results[results.length-2][1]) {
      // console.log("We're tipping");
      // use the median
      current_font_size = (Math.min(current_font_size, results[results.length-2][0]) * 0.8) + (Math.max(current_font_size, results[results.length-2][0]) * 0.2);
      break;
    } else {
      current_font_size = current_font_size * (target_font_size / rendered_font_size);
    }
    rendered_font_size = determineFontHeight(font, current_font_size);
    results.push([current_font_size, rendered_font_size]);
    // If we're going back and forth over the target
  }
  // console.log("settled at", current_font_size, "=", rendered_font_size);
  return current_font_size;
}

export const clearBitmapToColor = function (bitmap, color) {
  bitmap.ctx.beginPath();
  bitmap.ctx.rect(0, 0, bitmap.width, bitmap.height);
  bitmap.ctx.fillStyle = color;
  bitmap.ctx.fill();
}

export const drawRotatedOnBitmap = function (bitmap, source, degrees, x = 0, y = 0) {
  if (degrees != 0) {
    let Cx = bitmap.width * 0.5,
        Cy = bitmap.height * 0.5;
    bitmap.context.translate(Cx, Cy);
    bitmap.context.rotate(degrees * DEGREES_TO_RADIANS);
    bitmap.context.translate(-Cx, -Cy);
    bitmap.context.drawImage(source, x, y);
    bitmap.context.resetTransform();
    // bitmap.context.setTransform(1, 0, 0, 1, 0, 0);
  } else {
    bitmap.context.drawImage(source, x, y);
  }
  return bitmap;
}

export const createCanvas = function (width, height) {
  interface CustomCanvas extends HTMLCanvasElement {
    context?: CanvasRenderingContext2D;
  }
  let canvas = <CustomCanvas>document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  canvas.context = canvas.getContext('2d');
  return canvas;
}

export const distanceBetweenPoints = function (x1, y1, x2, y2) {
  return Math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
}

export const angleToEdgePoint = function (width, height, angle) {
  let x = 0,
      y = 0,
      x_factor = 1,
      y_factor = 1,
      rect_atan = Math.atan2(height, width),
      region = 4
  ;
  // Make sure angle is in range -PI - PI
  while (angle < -Math.PI) {
    angle += TWO_PI;
  }
  while (angle > Math.PI) {
    angle -= TWO_PI;
  }

  if ((angle > -rect_atan) && (angle <= rect_atan)) {
    region = 1;
  } else if ((angle > rect_atan) && (angle <= (Math.PI - rect_atan))) {
    region = 2;
  } else if ((angle > (Math.PI - rect_atan)) || (angle <= -(Math.PI - rect_atan))) {
    region = 3;
  } // else region = 4

  switch (region) {
    case 1: y_factor = -1; break;
    case 2: y_factor = -1; break;
    case 3: x_factor = -1; break;
    case 4: x_factor = -1; break;
  }

  if ((region == 1) ||  (region == 3)) {
    x += x_factor * (width / 2);
    y += y_factor * (width / 2) * Math.tan(angle);
  } else {
    // region 2 or 4
    x += x_factor * (height / (2 * Math.tan(angle)));
    y += y_factor * (height /  2);
  }

  return [x, y];
}