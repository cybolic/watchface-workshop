// import jetpack from 'fs-jetpack';
// import fs from 'fs-extra';
import * as Promise from 'bluebird'
import fs from 'fs';

export const mapObject = function (obj, func) {
  return Object.keys(obj).reduce(function(previous, current) {
    previous[current] = func(current, obj[current]);
    return previous;
  }, {});
};


export const getTempString = function () {
  return (+new Date()).toString(36)+(Math.random()*100).toString(36);
}

export const zeroPad = function(number, length) {
  return (Array(length).join('0')+number).slice(- length);
}

export const imageToDataURL = function(image) {
  // Create an empty canvas element
  var canvas = document.createElement('canvas');
  canvas.width = image.width;
  canvas.height = image.height;

  // Copy the image contents to the canvas
  var ctx = canvas.getContext('2d');
  ctx.drawImage(image, 0, 0);

  // Get the data-URL formatted image
  var dataURL = canvas.toDataURL('image/png');

  canvas = ctx = null;
  return dataURL;
};

// Load an image, call back with an Image object
export const loadImage = function(path, callback) {
  // jetpack.exists(path, (result) => {
  fs.access(path, fs.constants.R_OK, (error) => {
    if (error) {
      return callback && callback("file is not readable.");
    }

    // Create IMG object and initiate loading
    let image = (<HTMLImageElement>document.createElement('IMG'));

    // When done loading, call back with image
    image.onload = () => {
      return callback && callback(null, image);
    };
    // If loading fails, call back with error
    image.onerror = (error) => {
      return callback && callback(error);
    };

    image.src = path;
  });
};

export const loadImageFromBase64 = function(data, filetype, no_image=false) : Promise.Thenable<any> {
  return new Promise((resolve, reject) => {
    if (filetype == 'jpg')
      filetype = 'jpeg';

    data = `data:image/${filetype};base64,${data}`;

    if (no_image == false) {
      // Create IMG object and initiate loading
      let image = (<HTMLImageElement>document.createElement('IMG'));

      // When done loading, call back with image
      image.onload = () => {
        resolve(image);
      };
      // If loading fails, call back with error
      image.onerror = (error) => {
        reject(error);
      }
      // Load image
      image.src = data;

    } else {
      resolve(data);
    }

  });
}