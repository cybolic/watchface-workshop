import { Base, Properties as BaseProperties, DrawProperties } from './base';


export interface TextRingProperties extends BaseProperties, DrawProperties {
  text_size : number;
  font : string;
  transform : string;
  radius : number;
  anim_scale_x : number;
  anim_scale_y : number;
  ring_type : string; // 1-12, 1-14, 1-30, 1-31, 1-60, 1-100, Custom (x to y), I-XII, Mo-Su, Mon-Sun, Jan-Dec
  show_every : number;
  hide_text : string;
  rotated_text : string; // upright, rotate, rotate inverse, rotate upright
  angle_start : number;
  angle_end : number;
  squarify : number; // 0-100
  // text effects (drop shadow, flat shadow, outline, glow)
  outline : 'Drop Shadow' | 'Flat Shadow' | 'Outline' | 'Glow';
  o_color : string;
  o_opacity : number;
  o_size : number;
}
export default class TextRing extends Base {
  properties: TextRingProperties;

  constructor(data) {
    super();
    this.properties.type = 'text_ring';
    this.properties.font = "Roboto-Regular";
    this.properties.color = 'ffffff';
    this.properties.color_dim = 'ffffff';
    this.properties.text_size = 30;
    this.properties.radius = 200;
    this.properties.transform = "n";
    this.properties.anim_scale_x = 100;
    this.properties.anim_scale_y = 100;
    this.properties.ring_type = "1-12";
    this.properties.show_every = 1;
    this.properties.hide_text = "";
    this.properties.rotated_text = "y";
    this.properties.angle_start = 0;
    this.properties.angle_end = 360;
    this.properties.squarify = 0;
    this.properties.outline = "Outline";
    this.properties.o_color = "000000";
    this.properties.o_opacity = 100;
    this.properties.o_size = 3;
    Object.keys(data).forEach((key) => {
      this.properties[key] = data[key];
    });
  }

  _init() {
  }
}