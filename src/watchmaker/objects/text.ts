import { Base, Properties as BaseProperties, DrawProperties } from './base';

const ALIGNMENT_TO_TEXT_ALIGNMENT = {
  'l': 'left',
  'c': 'center',
  'r': 'right'
};

export interface TextProperties extends BaseProperties, DrawProperties {
  text : string;
  text_size : number;
  font : string;
  transform : string;
}
export default class Text extends Base {
  properties : TextProperties;

  constructor(data) {
    super();
    this.properties.type = 'text';
    this.properties.color = 'ffffff';
    this.properties.color_dim = 'ffffff';
    this.properties.text = "";
    this.properties.text_size = 20;
    this.properties.font = "";
    this.properties.transform = '';

    // Chrome can't use fonts that begin with a dot
    if (data['font'] != null && data['font'][0] == '.')
      data['font'] = data['font'].replace(/^\./, '');

    Object.keys(data).forEach((key) => {
      this.properties[key] = data[key];
    });
  }

  getAlignment() {
    return ALIGNMENT_TO_TEXT_ALIGNMENT[this.properties.alignment[1]];
  }

  getTransformed(text) {
    text = text || this.calculatedProperties.text || this.properties.text;
    if (this.calculatedProperties.transform != '') {
      switch (this.calculatedProperties.transform) {
        case 'uc':
          return text.toUpperCase();
        default:
          return text;
      }
    } else {
      return text;
    }
  }

  _init() {
  }
}