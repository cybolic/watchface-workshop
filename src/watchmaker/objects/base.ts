const ALIGNMENT_TO_ANCHOR = {
  't': 0.0,
  'l': 0.0,
  'b': 1.0,
  'r': 1.0,
  'c': 0.5
};

export interface Properties {
  type : string;
  name? : string;
  x : number;
  y : number;
  gyro : number;
  rotation : number;
  skew_x : number;
  skew_y : number;
  opacity : number;
  alignment : 'tl' | 'tc' | 'tr' | 'cl' | 'cc' | 'cr' | 'bl' | 'bc' | 'br';
  display : {
    bright : boolean,
    dimmed : boolean
  };
  visible : boolean;
}
export interface SizedProperties {
  width : number;
  height : number;
}
export interface ColorProperties {
  color : string;
}
export interface DrawProperties extends ColorProperties {
  color_dim : string;
}
export interface ShaderProperties {
  shader : string;
}

export class Base {
  properties : Properties;
  calculatedProperties : any = {};
  offset : [number, number] = [0, 0];

  constructor() {
    this.properties = {
      type: 'base',
      name: null,
      x: 0,
      y: 0,
      gyro: 0,
      rotation: 0,
      skew_x: 0,
      skew_y: 0,
      opacity: 100,
      alignment: 'cc',
      display: {
        bright: true,
        dimmed: true
      },
      visible: true,
    }
  }

  getAnchor () {
    return [
      this.getAnchorX(),
      this.getAnchorY()
    ];
  }
  getAnchorX () {
     return ALIGNMENT_TO_ANCHOR[this.properties['alignment'][1]];
  }
  getAnchorY () {
     return ALIGNMENT_TO_ANCHOR[this.properties['alignment'][0]];
  }

  getXYCoordinates (width, height) {
    return [
      this.getXCoordinate(width),
      this.getYCoordinate(height)
    ];
  }
  getXCoordinate (width) {
    return (width|0)  * 0.5 + (this.properties.x|0);
  }
  getYCoordinate (height) {
    return (height|0)  * 0.5 + (this.properties.y|0);
  }

}