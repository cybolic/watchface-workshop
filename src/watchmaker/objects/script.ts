/// <reference path="../../lib/@types/lua.vm.js.d.ts" />
import LuaVM from 'lua.vm.js';
import fs from 'fs';
import moment from 'moment-timezone';
// import { remote } from 'electron';
import * as utils from '../utils';
import tags from '../tags';


const RE_TEMPLATE_VARIABLES       = new RegExp(`\{(${Object.keys(tags).join('|')})\}`, 'g');
const RE_TEMPLATE_VARIABLE        = new RegExp(`^\{(${Object.keys(tags).join('|')})\}$`, '');
const RE_TEMPLATE_WMSCHEDULE      = new RegExp(`wm_schedule\\s*\{(.*?)\}`, 'g');
const RE_TEMPLATE_WMSCHEDULE_ARGS = new RegExp(``, 'g');
const RE_TEMPLATE_COMMENTS        = new RegExp(`--\\[\\[[^]*?--\\]\\]`, 'gm');

export default class Script {
  environment : any;
  moment : moment.Moment;
  lua : any;

  constructor() {
    this.environment = {};
    this.moment = moment();
    this.lua = new LuaVM.Lua.State();

    let luaEasing = fs.readFileSync(__dirname+'/easing.lua', 'utf8');
    this.lua.execute(luaEasing);

    this.setEnvironment(tags)
  }

  setScriptVariable(variable, value) {
    this.lua._G.set(variable, value);
  }
  getScriptVariable(variable) {
    return this.lua._G.get(variable);
  }

  runString(string) {
    string = this._replaceTemplateVariablesInString(string);
    string = this._replaceWMScheduleFunctionsInString(string);
    string = this._removeCommentsInString(string);
    // console.log(string);
    this.lua.execute(string);
    // console.log("Loaded script.");
  }

  setEnvironment(env) {
    Object.keys(env).forEach((property) => {
      this.setScriptVariable(`__${property}`, env[property]);
    });
    this.environment = env;
  }

  _replaceTemplateVariablesInString(string) {
    // Replace {} type variables with underscore prefixed normal variables
    // so the function can pick up the value from the global scope
    return string.replace(RE_TEMPLATE_VARIABLES, (match, variable_name) => {
      if (this.environment[variable_name] != null) {
        return `__${variable_name}`;
      } else {
        return '0'
      }
    });
  }

  _calcTemplateVariablesInString(string) {
    // Replace {} type variables with environment variables
    return string.replace(RE_TEMPLATE_VARIABLES, (match, variable_name) => {
      // If we have a calulated variable for this, return it
      if (this.environment[variable_name] != null) {
        return this.environment[variable_name];
      // Otherwise, leave it alone
      } else {
        return match;
      }
    });
  }

  _replaceWMScheduleFunctionsInString(string) {
    return string.replace(RE_TEMPLATE_WMSCHEDULE, (match, args) => {
      let kwargs={};
      args.split(',').map((key) => {
        let parts = key.split('=');
        kwargs[parts[0].trim()] = parts[1].trim();
      });

      switch (kwargs['action']) {
        case '"tween"':
        case "'tween'":
          return `wm_schedule_tween(${kwargs['tween']}, ${kwargs['from']}, ${kwargs['to']}, ${kwargs['duration']}, ${kwargs['easing']})`;
      }

    });
  }

  _removeCommentsInString(string) {
    return string.replace(RE_TEMPLATE_COMMENTS, '');
  }

  parseVariable(variable) {
    if (typeof variable == 'function') {
      return variable();
    } else if (typeof variable == 'string') {
      if (RE_TEMPLATE_VARIABLE.test(variable)) {
        return this._calcTemplateVariablesInString(variable);
      } else {
        let function_string = this._calcTemplateVariablesInString(variable);
        try {
          return this.lua.execute('return ' + function_string)[0];
        } catch (error) {
          return function_string;
        }
      }
    } else {
      return variable;
    }
  }

  update() {
    let current_date = new Date(),
        environment = Object.assign({}, tags);
    ;
    this.moment = moment();
    // environment.dh24 = current_date.getHours();
    // environment.dm   = current_date.getMinutes();
    // environment.ds   = current_date.getSeconds();
    // environment.dss  = current_date.getMilliseconds();
    // environment.drs  = environment.ds / 60 * 360;
    // environment.drss  = (environment.ds+environment.dss*0.001) / 60 * 360;
    // environment.drms  = environment.dss / 1000 * 360;
    // // environment.dh24 = time/1000 / 3600;
    // environment.dh   = parseInt(environment.dh24 % 12);
    // // environment.dm   = time/1000/60 / 60  - environment.dh24 * 60;
    // // environment.ds   = time/1000/60 - environment.dm * 60 * 60;
    // // environment.drms = parseInt(environment.drss);

    // environment.wt = 13; // current temperature


    environment.dd = current_date.getDate(); // Day in month
    environment.ddz = utils.zeroPad(environment.dd, 2); // Day in month (with leading zero)
    environment.ddy = this.moment.dayOfYear(); // Day in year
    environment.ddw2 = this.moment.format('dd'); // Day of week
    environment.ddw = this.moment.format('ddd'); // Day of week
    environment.ddww = this.moment.format('dddd'); // Day of week
    environment.ddw0 = current_date.getDay(); // Day of week (Sun = 0, Sat = 6)
    environment.ddim = this.moment.daysInMonth(); // Days in current month
    environment.dn = current_date.getMonth()+1; // Month in year
    environment.dnn = utils.zeroPad(environment.dn, 2); // Month in year
    environment.dnnn = this.moment.format('MMM'); // Month in year
    environment.dnnnn = this.moment.format('MMMM'); // Month in year
    environment.dyy = current_date.getFullYear(); // Year (4 digits)
    environment.dy = parseInt(String(environment.dyy).slice(2)); // Year (2 digits)
    environment.dwm = Math.ceil(this.moment.date() / 7); // Week in month
    environment.dw = this.moment.week(); // Week in year


  /* Time Tags */

    environment.dh24 = current_date.getHours(); // Hour in day (1-24)
    environment.dh23 = environment.dh24-1; // Hour in day (0-23)
    environment.dh = parseInt(String(environment.dh24 % 12)); // Hour in day (1-12)
    environment.dh11 = environment.dh-1; // Hour in day (0-11)
    environment.dht = 'nine'; // Hour in day text (1-12)
    environment.dh24t = 'twenty-one'; // Hour in day text (1-24)
    environment.dhz = '09'; // Hour in day (1-12) (with leading zero)
    environment.dh11z = '21'; // Hour in day (0-11) (with leading zero)
    environment.dh24z = '09'; // Hour in day (1-24) (with leading zero)
    environment.dh23z = '21'; // Hour in day (0-23) (with leading zero)
    environment.dm = this.moment.minute(); // Minute in hour
    environment.dmz = utils.zeroPad(environment.dm, 2); // Minute in hour (with leading zero)
    environment.dhtt = 0; // Hour in day (1-12 tens)
    environment.dhto = 9; // Hour in day (1-12 ones)
    environment.dh11tt = 0; // Hour in day (0-11 tens)
    environment.dh11to = 9; // Hour in day (0-11 ones)
    environment.dh24tt = 0; // Hour in day (1-24 tens)
    environment.dh24to = 9; // Hour in day (1-24 ones)
    environment.dh23tt = 0; // Hour in day (0-23 tens)
    environment.dh23to = 9; // Hour in day (0-23 ones)
    environment.dmt = 3; // Minute in hour (tens)
    environment.dmo = 1; // Minute in hour (ones)
    environment.dmat = 'thirty one'; // Minute in hour text (all)
    environment.dmtt = 'thirty'; // Minute in hour text (tens)
    environment.dmot = 'one'; // Minute in hour text (ones)
    environment.ds = current_date.getSeconds(); // Second in minute
    environment.dsz = '02'; // Second in minute (with leading zero)
    environment.da = this.moment.format('A'); // AM/PM
    environment.dss = current_date.getMilliseconds(); // Milliseconds
    environment.dssz = this.moment.format('SSS'); // Milliseconds (with leading zeros)
    environment.dsps = environment.ds * 1000 + environment.dss; // Seconds * 1000 + milliseconds
    environment.dz = this.moment.format('z'); // Timezone
    environment.dtp = 0.74; // Time (% 24 hours)
    environment.drh = Math.floor((environment.dh11 + environment.dm/60) / 11 * 360); // 290; // Rotation value for hour (12h, adj for mins)
    environment.drh24 = Math.floor((environment.dh23 + environment.dm/60) / 23 * 360); // 145; // Rotation value for hour hand (24h, adj for mins)
    environment.drh0 = Math.floor(environment.dh11 / 11 * 360); // 270; // Rotation value for hour hand (12h)
    environment.drm = Math.floor((environment.dm + environment.ds/60) / 60 * 360); // 242; // Rotation value for min hand (adj for secs)
    environment.drs = Math.floor(environment.ds / 60 * 360); // Rotation value for second hand
    environment.drss = (environment.ds+environment.dss*0.001) / 60 * 360; // Rotation smooth value for second hand
    environment.drms = Math.floor(environment.dss / 1000 * 360); // Rotation value for milliseconds


    // console.log(environment);

    this.setEnvironment(environment);
  }

  calcLayerProperties(layer) {
    // layer.calculatedProperties = utils.mapObject(layer.properties, (key, value) => {
    if (layer.calculatedProperties === undefined) layer.calculatedProperties = {};
    if (layer.ignoredProperties === undefined) layer.ignoredProperties = {};
    Object.keys(layer.properties).forEach(key => {
      // Ignore properties that have previously failed computation
      if (layer.ignoredProperties[key]) {
        // Is the value still the one that fails?
        if (layer.ignoredProperties[key] == layer.properties[key]) {
          layer.calculatedProperties[key] = layer.properties[key];
          return
        // If it's changed, try the new value and remove the flag
        } else {
          delete layer.ignoredProperties[key];
        }
      }
      switch (key) {
        case 'name':
        case 'type':
        case 'path':
        case 'alignment':
        case 'gyro':
        case 'color':
        case 'color_dim':
        case 'display':
        case 'font':
        case 'shape':
        case 'shader':
        case 'tap_action':
          layer.calculatedProperties[key] = layer.properties[key];
          break;
        default:
          try {
            layer.calculatedProperties[key] = this.parseVariable(layer.properties[key]);
            if (layer.calculatedProperties[key] == undefined) {
              layer.calculatedProperties[key] = layer.properties[key];
            }
          } catch (error) {
            console.error("Property value script error in layer:", layer.name, "property:", key, "value:", layer.properties[key], "error:", error);
            layer.calculatedProperties[key] = layer.properties[key];
            layer.ignoredProperties[key] = layer.properties[key];
          }
          break;
      }
    });
  }
}