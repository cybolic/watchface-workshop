import { Base, Properties as BaseProperties, DrawProperties, SizedProperties, ShaderProperties } from './base';

export interface MarkersProperties extends BaseProperties, DrawProperties, SizedProperties, ShaderProperties {
  shape : 'Square' | 'Circle' | 'Triangle' | 'Pentagon' | 'Hexagon' | 'Star' | 'Heart';
}
export default class Shape extends Base {
  properties: MarkersProperties;

  constructor(data) {
    super();
    this.properties.type = 'shape';
    this.properties.color = 'ffffff';
    this.properties.color_dim = 'ffffff';

    this.properties.width = 0;
    this.properties.height = 0;
    this.properties.shader = '';
    this.properties.shape = "Square";
    Object.keys(data).forEach((key) => {
      this.properties[key] = data[key];
    });
  }

  _init() {
  }
}