import * as Promise from 'bluebird';
import xml2js from 'xml2js';
import path from 'path';
import { Base as Layer } from './base';
import Image from './image';
import Text from './text';
import TextRing from './text_ring';
import Shape from './shape';
import Markers from './markers';
import Script from './script';
import * as utils from '../utils';
import * as draw_utils from '../draw_utils';

const LOADED_FONTS = [
  'BebasNeue',
  'CaviarDreams_Bold',
  'CaviarDreams_BoldItalic',
  'CaviarDreams_Italic',
  'CaviarDreams',
  'LEDFont',
  'Roboto-Black',
  'Roboto-BlackItalic',
  'Roboto-Bold',
  'Roboto-BoldCondensed',
  'Roboto-BoldCondensedItalic',
  'Roboto-BoldItalic',
  'Roboto-Condensed',
  'Roboto-CondensedItalic',
  'Roboto-Italic',
  'Roboto-Light',
  'Roboto-LightItalic',
  'Roboto-Medium',
  'Roboto-MediumItalic',
  'Roboto-Regular',
  'Roboto-Thin',
  'Roboto-ThinItalic',
  'space age'
];

interface ImageArray {
  [path: string]: HTMLImageElement;
}

export default class Watch {
  properties : any = {};
  // layers : Array<Layer | Image | Text | TextRing | Shape | Markers> = [];
  layers : Array<any> = [];
  images : ImageArray = {};
  script : Script = new Script();
  scriptString: string;
  preview : string; // data URL
  edited : boolean = false;

  constructor() {
    this.properties = {
      'name': "",
      'author': "",
      'description': "",
      'features': "",
      'web_link': "",
      'bg_color': '000000',
      'hotword_bg': true,
      'hotword_loc': 'tc',
      'ind_bg': false,
      'ind_loc': 'cc'
    };
  }

  getLayersByPath(path) {
    return this.layers.filter((layer) => {
      return layer.properties.path && layer.properties.path == path;
    });
  }

  loadFromXMLString(xml_string) {
    // Parse the XML string into an object
    return new Promise((resolve, reject) => {
      xml2js.parseString(xml_string, (error, object) => {
        if (error != null) return reject(error);
        if (object['Watch'] == null || object['Watch']['$'] == null)
          return reject(new Error('No Watch object found in XML. Aborting.'));

        return resolve(object);
      });
    })
    // Read the main watch properties
    .then(object => {
      Object.keys(this.properties).concat(Object.keys(object['Watch']['$'])).forEach((key) => {
        if (object['Watch']['$'][key] != null) {
          this.properties[key] = object['Watch']['$'][key];
        }
      });

      if (this.properties.protected != null && (this.properties.protected == 'y' || this.properties.protected == 'Y' || this.properties.protected == true)) {
        console.log("Error");
        throw new Error("This watch face is protected. Can't load.");
      }

      console.log('Watch:', object['Watch']);
      return object;
    })
    // Read the layers and wait for any resources to load
    .then((object) => {
      this.layers = [];
      if (object['Watch']['Layer'] != null && object['Watch']['Layer'].length) {

        return Promise.each(object['Watch']['Layer'], (layer, index) => {
          // console.log("Looking at layer", index);
        // object['Watch']['Layer'].forEach((layer) => {
          if (layer['$'] != null) {
            // Read layer data and fix or touch up any troublesome parts
            return new Promise((resolve, reject) => {
              // console.log("Reading layer properties");
              let data = layer['$'];
              // Fix data
              if (typeof data['font'] == 'string') {
                // Chrome can't use font names that begin with a dot, so remove any leading dot
                data['font'] = data['font'].replace(/^\./, '');

                if (data['font'].toLowerCase() == 'ledfont-dg') {
                  data['font'] = 'LEDFont';
                }
              }
              resolve(data);
            })
            // Load resources
            .then(data => {
              // console.log("Loading layer resources");
              if (typeof data['font'] == 'string') {
                return new Promise((resolve, reject) => {
                  draw_utils.waitForFontLoad(data['font'])
                  .then(
                    () => {
                      resolve(data);
                    }, () => {
                      console.error("Couldn't seem to load font:", data['font']);
                      resolve(data);
                    }
                  );
                });
              } else {
                return data;
              }
            })
            // Create layer objects
            .then(data => {
              // console.log("Creating layer object");
              switch (data['type']) {
                case 'image':
                  data['path'] = path.join('images', data['path']);
                  this.layers.push(new Image(data));
                  break;
                case ('text'):
                  this.layers.push(new Text(data));
                  break;
                case ('text_ring'):
                  this.layers.push(new TextRing(data));
                  break;
                case ('shape'):
                  this.layers.push(new Shape(data));
                  break;
                case ('markers'):
                  this.layers.push(new Markers(data));
                  break;
                case ('markers_hm'):
                  this.layers.push(new Markers(data));
                  break;
              }
              return data;
            });
          }
        });
      } else {
        // resolve();
        return;
      }
    })
    // Return triumphant
    .then( () => {
      console.log("Loaded layers");
      return this;
    });
  }

  loadImageFromBase64(path, base64_string) {
    return utils.loadImageFromBase64(base64_string, path.split('.').pop())
      .then((image : HTMLImageElement) => {
        this.images[path] = image;

        this.layers.forEach((layer) => {
          if (layer.properties.type == 'image' && layer.properties.path == path) {
            layer.setFromImage(image);
          }
        });
      })
    ;
  }

  // Returns Promise that resolves to a base64 string
  loadPreviewFromBase64(path, base64_string) {
    return utils.loadImageFromBase64(base64_string, path.split('.').pop(), true)
      .then((data_url : string) => {
        this.preview = data_url;
      })
    ;
  }

  loadFontFromBase64(path, base64_string) {
    // Get extension (format) and base name
    let format = path.match(/\.[a-z]+$/i)[1],
        name = (path.split('/').pop()).replace(/\.[a-z]+$/i, '');
    // Chrome can't use font names that begin with a dot, so remove any leading dot
    name = name.replace(/^\./, '');
    switch (format) {
      case 'otf':
        base64_string = `data:application/x-font-otf;charset=utf-8;base64,${base64_string}`;
        format = 'opentype';
        break;
      default:
        // The correct mime-type is application/x-font-ttf but Webkit/Blink is more lenient with font/ttf
        base64_string = `data:font/ttf;charset=utf-8;base64,${base64_string}`;
        format = 'truetype';
        break;
    }
    console.log("Loading font:", name, format);
    return this.loadFont(name, base64_string, format);
  }

  getFontIdFromName(name) {
    // Replace unwieldly characters
    return name.replace(/[^a-zA-Z0-9\-]/g, '_');
  }

  loadFont(name, path, format = 'truetype') {
    if (LOADED_FONTS.indexOf(name) == -1) {
      let style = document.createElement('style');
      style.type = 'text/css';
      style.id = 'font-'+this.getFontIdFromName(name);
      style.appendChild(
        document.createTextNode(
          `@font-face {
            font-family: "${name}";
            src: url('${path}') format('${format}');
          }`
        )
      );
      document.querySelector('head').appendChild(style);

      let el = document.createElement('div');
      el.appendChild(document.createTextNode(`a-zA-Z0-9`));
      style.id = 'font-trigger-'+this.getFontIdFromName(name);
      el.style.cssText = `position: absolute; z-index: -1; top: 0; left: 0; font-size: 8px; font-family: "${name}"`;
      document.querySelector('body').appendChild(el);
      // disabled because it doesn't actually return a promise
      // but an uncatchable error when it fails
      // let promise = document.fonts.load(`12px ${name}`);
      LOADED_FONTS.push(name);
      // Return Promise so we can see when it's loaded
      return Promise.resolve();
    } else {
      return Promise.resolve();
    }
  }

  loadScriptFromString(string) {
    this.scriptString = string;
    this.script.runString(string);
  }
}