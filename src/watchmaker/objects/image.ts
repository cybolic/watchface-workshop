import { Base, Properties as BaseProperties, SizedProperties, ColorProperties, ShaderProperties } from './base';

// import jetpack from 'fs-jetpack';
import * as utils from '../utils';

export interface ImageProperties extends BaseProperties, SizedProperties, ColorProperties, ShaderProperties {
  path? : string;
}
export default class Image extends Base {
  properties : ImageProperties;
  image : HTMLImageElement;
  loaded : boolean = false;

  constructor(data) {
    super();
    this.properties.type = 'image';
    this.properties.path = null;
    this.properties.width = null;
    this.properties.height = null;
    this.properties.color = 'ffffff';
    this.properties.shader = '';
    Object.keys(data).forEach((key) => {
      this.properties[key] = data[key];
    });
  }

  setFromImage(image) {
    this.loaded = true;
    this.image = image;
    if (this.properties['width'] == null) {
      this.properties['width'] = parseInt(String(this.image.width));
      this.image.width = this.properties['width'];
    }
    if (this.properties['height'] == null) {
      this.properties['height'] = parseInt(String(this.image.height));
      this.image.height = this.properties['height'];
    }

  }

  loadFromBase64(data, filetype, callback?) {
    // Create IMG object and initiate loading
    let image = (<HTMLImageElement>document.createElement('IMG'));
    image.src = `data:image/${filetype};base64,${data}`;

    // When done loading, set properties and possible image size
    image.onload = () => {
      this.setFromImage(image);
      return callback && callback(null, image);
    };
    // If loading fails, call back with error
    image.onerror = (error) => {
      return callback && callback(error);
    };
  }

  loadFile(path, callback?) {
    utils.loadImage(path, (error, image) => {
      if (error)
        return callback && callback(error);

      this.properties.path = path;
      this.setFromImage(image);
      return callback && callback
    });
  }

  _init() {
  }
}