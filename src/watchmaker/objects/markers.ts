import { Base, Properties as BaseProperties, DrawProperties  } from './base';

export interface MarkersProperties extends BaseProperties, DrawProperties  {
  radius : number;
  anim_scale_x : number;
  anim_scale_y : number;
  m_width : number;
  m_height : number;
  m_count : number;
  shape : 'Square' | 'Circle' | 'Triangle';
  squarify : number; // 0-100
}
export default class Markers extends Base {
  properties: MarkersProperties;

  constructor(data) {
    super();
    this.properties.type = 'markers';
    this.properties.color = 'ffffff';
    this.properties.color_dim = 'ffffff';
    this.properties.radius = 256;
    this.properties.anim_scale_x = 100;
    this.properties.anim_scale_y = 100;
    this.properties.m_width = 5;
    this.properties.m_height = 35;
    this.properties.m_count = 12;
    this.properties.shape = "Square";
    this.properties.squarify = 0;
    Object.keys(data).forEach((key) => {
      this.properties[key] = data[key];
    });
  }

  _init() {
  }
}
