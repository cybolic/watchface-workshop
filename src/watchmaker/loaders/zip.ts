import * as Promise from 'bluebird';
import fs from 'fs';
import EventEmitter from 'eventemitter3';
import JSZip from 'jszip';
import Watch from '../objects/watch';

export const load = function(path, callback?) {
  let watch = new Watch();
  return new Promise((resolve, reject) => {
    fs.readFile(path, function(error, data) {
      if (error) {
        callback && callback(error);
        return reject(error);
      }
      (new JSZip).loadAsync(data).then(function (zip) {
        zip = zip;
        if (zip.files['watch.xml'] == null) {
          callback && callback('No XML file found in archive. Aborting.');
          return reject(new Error('No XML file found in archive. Aborting.'));
        } else {
          resolve(zip);
        }
      });
    });
  })
  .then( (zip : JSZip) => {
    return Promise.each(Object.keys(zip.files), (filename) => {
      // console.log("Checking file:", filename);
      if (filename.indexOf('images/') == 0 && filename != 'images/') {
        let ext = filename.split('.').pop();
        if (['png','jpg','jpeg','gif'].indexOf(filename.split('.').pop()) != -1) {
          return zip.files[filename].async('base64')
          .then((base64_string) => {
            return watch.loadImageFromBase64(filename, base64_string);
          });
        } else if (filename.split('.').pop().toLowerCase() == 'ppng') {
          throw new Error("This watch face is protected. Can't load.");
        } else {
          console.log("Unknown filetype for image, skipping:", filename);
        }
      } else if (filename.indexOf('fonts/') == 0 && filename != 'fonts/') {
        return zip.files[filename].async('base64')
        .then(
          (base64_string) => {
            return watch.loadFontFromBase64(filename, base64_string);
          }, (error) => {
            console.error("Couldn't load the font:", filename);
          }
        );
      } else if (filename == 'scripts/script.txt') {
        return zip.files[filename].async('string')
        .then((string) => {
          watch.loadScriptFromString(string);
        });
      } else if (filename == 'preview.jpg') {
        return zip.files[filename].async('base64')
        .then((string) => {
          watch.loadPreviewFromBase64(filename, string);
        });
      }
    })
    .then(() => {
      return zip;
    });
    // console.log("Done loading files");
  })
  .then((zip) => {
    console.log(zip)
    // Load XML file
    return zip.files['watch.xml']
    .async('string')
    .then((xml_string) => {
      return watch.loadFromXMLString(xml_string)
    })
    .then(() => {
      // Set loaded image to the layers that request them
      Object.keys(watch.images).forEach((path) => {
        watch.layers.forEach((layer) => {
          if (layer.properties.type == 'image' && layer.properties.path == path) {
            layer.setFromImage(watch.images[path]);
          }
        });
      });
      // Since the last promise gave us a string, switch back to the zip
      return zip;
    });
  })
  .then(() => {
    callback && callback(null, watch);
    return Promise.resolve(watch);
  });
};