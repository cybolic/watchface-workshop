import os from 'os';
import { remote, ipcRenderer as ipc } from 'electron';
// import jetpack from 'fs-jetpack';
// import fs from 'fs-extra';
import fs from 'fs';
import env from './env';

import { getSystemFont } from './electron_utils';

// var appDir = jetpack.cwd(remote.app.getAppPath());

const systemFont = getSystemFont();

// console.log('The author of this app is:', appDir.read('package.json', 'json').author);
// console.log('The author of this app is:', fs.readJsonSync(remote.app.getAppPath()+'/package.json').author);
console.log('The author of this app is:', JSON.parse(fs.readFileSync(remote.app.getAppPath()+'/package.json', {encoding:'utf8'})).author);

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
const platform = platformBrowserDynamic();

document.addEventListener('DOMContentLoaded', function() {
  document.body.style.fontFamily = systemFont['family'];
  document.body.style.fontSize = systemFont['size']+'px';
  platform.bootstrapModule(AppModule);
});