import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { TooltipModule } from 'ng2-bootstrap';
import { PropertyEditorComponent } from './directives/property-editor';
import { WindowPropertiesComponent } from './window-properties.component';

@NgModule({
  imports:      [ BrowserModule, TooltipModule ],
  declarations: [ WindowPropertiesComponent, PropertyEditorComponent ],
  bootstrap:    [ WindowPropertiesComponent ]
})
export class WindowPropertiesModule { }