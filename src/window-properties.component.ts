import { remote, ipcRenderer as ipc } from 'electron';
import { Component, NgZone } from '@angular/core';
import { getWindowByName } from './electron_utils';

@Component({
  selector: 'wb-properties',
  templateUrl: 'templates/window-properties.html',
  host: {
    'class': 'c-watch-properties'
  }
})
export class WindowPropertiesComponent {
  properties : any = {};

  private mainWindow = getWindowByName('main');

  constructor (private zone: NgZone) {
    ipc.on('set-properties', (emitter, properties : any) => {
      if (properties.bg_color != null) {
        properties.bg_color = '#'+properties.bg_color;
      }
      this.properties = properties;
    });
    ipc.on('show-properties', (emitter, properties : any) => {
      this.zone.run(() => {});
    });
  }

  onModelChange (name, value) {
    this.mainWindow.webContents.send('change-watch-property', name, value);
  }


  updateBackgroundColor (color : string) {
    if (! color.match(/^#/)) {
      color = '#'+color;
    }
    this.mainWindow.webContents.send('change-watch-background-color', color);
  }
}