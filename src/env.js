// Simple wrapper exposing environment variables to rest of the code.

// import jetpack from 'fs-jetpack';
// import fs from 'fs-extra';
import fs from 'fs';

// The variables have been written to `env.json` by the build process.
// var env = jetpack.cwd(__dirname).read('env.json', 'json');
var env = JSON.parse(fs.readFileSync(__dirname+'/env.json', {encoding:'utf8'}));

export default env;
