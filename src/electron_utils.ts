import { remote } from 'electron';
import fs from 'fs';
import { execSync } from 'child_process';

export const showOpenDialog = function (options = {}) {
  return new Promise((resolve, reject) => {
    remote.dialog.showOpenDialog(remote.getCurrentWindow(), Object.assign({
      title: "Select a file to open",
      properties: ['openFile']
    }, options), resolve);
  });
};
export const showOpenFileDialog = showOpenDialog;

export const showOpenDirectoryDialog = function (options = {}) {
  return showOpenDialog(Object.assign({
    title: "Select a folder to open",
    properties: ['openDirectory']
  }, options));
}

export interface SystemFontInfo {
  family : string,
  size : number
}
export const getSystemFont = function() : SystemFontInfo {
  let font_info : any = {};
  let font_status : any;
  // KDE
  // if (process.env.KDE_SESSION_UID != null && jetpack.exists(process.env.HOME+'/.config/kdeglobals') == 'file') {
  //   font_status = jetpack.read(`${process.env.HOME}/.config/kdeglobals`).match(/\[General\][^]*?font=(.*)/);
  // if (process.env.KDE_SESSION_UID != undefined && fs.ensureFileSync(process.env.HOME+'/.config/kdeglobals')) {
  if (process.env.KDE_SESSION_UID != undefined && fs.accessSync(process.env.HOME+'/.config/kdeglobals', fs.constants.R_OK)) {
    font_status = fs.readFileSync(`${process.env.HOME}/.config/kdeglobals`, {encoding:'utf8'}).match(/\[General\][^]*?font=(.*)/);
    if (font_status) {
      font_status = font_status[1];
      font_status = font_status.split(',');
      font_info.family = font_status[0];
      font_info.size = parseFloat(font_status[1]) * 1.25;
    }
  // FontConfig
  } else {
    let fontconfig_installed = false;
    try {
      fontconfig_installed = execSync(`which fc-match`, {encoding: 'utf8'}) != '';
    } catch(e) {}
    if (fontconfig_installed) {
      font_status = execSync(`fc-match --verbose sans`, {encoding: 'utf8'});
      let match = font_status.match(/fullname:\s*(?:'(.*?)'|"(.*?)")/);
      if (match) {
        font_info.family = match[2];
      }
      match = font_status.match(/pixelsize:\s*(.*?)\(/);
      if (match) {
        font_info.size = parseFloat(match[1]);
      }
    }
  }
  return font_info
}

export const getWindowByName = function (name) {
  let windows = remote.BrowserWindow.getAllWindows().filter(window => {
    // We've added the name property to windows in the window helper module
    return (<any>window).name == name
  });
  if (windows.length) {
    return windows[0];
  } else {
    throw "No such window found: '"+name+"'";
  }
}