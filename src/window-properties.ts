import { getSystemFont } from './electron_utils';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { WindowPropertiesModule } from './window-properties.module';

const platform = platformBrowserDynamic();
const systemFont = getSystemFont();

document.addEventListener('DOMContentLoaded', function() {
  document.body.style.fontFamily = systemFont['family'];
  document.body.style.fontSize = systemFont['size']+'px';
  platform.bootstrapModule(WindowPropertiesModule);
});