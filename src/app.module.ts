import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { ButtonsModule, DropdownModule, TooltipModule } from 'ng2-bootstrap';

import { WatchService } from './watch.service';

import { AppComponent }     from './app.component';
import { ToolbarComponent } from './directives/toolbar.component';
import { WatchEditorComponent } from './directives/watch-editor.component';
import { EditorLayersComponent } from './directives/editor-layers';
import { EditorLayerComponent } from './directives/editor-layer';
import { EditorLayerPropertiesComponent } from './directives/editor-layer-properties';
// import { EditorLayerPropertyComponent } from './directives/editor-layer-property';
import { PropertyEditorComponent } from './directives/property-editor';
import { UISplitViewComponent } from './directives/ui-splitview';

@Pipe({ name: 'keys',  pure: false })
export class KeysPipe implements PipeTransform {
  transform(value: any, args: any[] = null): any {
    return Object.keys(value);
  }
}

@Pipe({ name: 'capitalise' })
export class CapitalisePipe implements PipeTransform {
  transform(value:string) : string {
    if (value) {
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
      return value;
  }
}

@NgModule({
  imports:      [ BrowserModule, FormsModule, ButtonsModule, DropdownModule, TooltipModule ],
  declarations: [
                  AppComponent, ToolbarComponent, WatchEditorComponent, KeysPipe, CapitalisePipe,
                  EditorLayersComponent, EditorLayerComponent, EditorLayerPropertiesComponent, PropertyEditorComponent,
                  UISplitViewComponent
                ],
  providers:    [ WatchService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }