declare namespace LuaVM {
  export namespace Lua {
    export function State() : void;
  }
}

declare module "lua.vm.js" {
  export = LuaVM;
}
// export = LuaVM;