import * as utils from './electron_utils';

import { Component } from '@angular/core';

import { WatchService } from './watch.service';

@Component({
  selector: 'my-app',
  templateUrl: 'templates/app.html',
  host: {
    'class': 'c-app'
  }
})
export class AppComponent {
  isRoundOverlayVisible: boolean = false;
  isPreviewOverlayVisible: boolean = false;
  isPropertiesVisible: boolean = false;
  loadedFile: string;
  loadedDir: string;

  constructor(public watchService: WatchService) {
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/alternate-gears.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/alternate-gears-builder.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/alternate-gears-v2-new-dials.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/steel-green.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/breitling-emergency.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/hexagen-segment.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/circular-bits.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/radar-watch-12h-animated.watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/testbed (1).watch');
    this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/testbed (2).watch');
    // this.onLoadFile('/home/cybolic/Projects/WatchCreator/app/watches/marker-test.watch');
  }

  get isUnchanged() : boolean {
    if (this.watchService.watch) {
      return this.watchService.watch.edited;
    } else {
      return true;
    }
  }

  onLoadFile(path: string) {
    console.log("Load watch file: "+path);
    this.watchService.loadFile(path)
    // this.loadedDir  = '';
    // this.loadedFile = path;
  }

  onLoadDir(path: string) {
    console.log("Load watch directory: "+path);
    this.watchService.loadDir(path);
    // this.loadedFile = '';
    // this.loadedDir  = path;
  }

  onRoundOverlayToggle() {
    this.isRoundOverlayVisible = ! this.isRoundOverlayVisible;
  }
  onPreviewOverlayToggle() {
    this.isPreviewOverlayVisible = ! this.isPreviewOverlayVisible;
  }
  // onPropertiesToggle() {
  //   this.isPropertiesVisible = ! this.isPropertiesVisible;
  // }
}